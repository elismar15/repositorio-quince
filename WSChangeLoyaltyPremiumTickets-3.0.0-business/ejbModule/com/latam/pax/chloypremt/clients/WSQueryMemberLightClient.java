package com.latam.pax.chloypremt.clients;

import java.net.URL;
import java.util.Properties;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.soap.SOAPFaultException;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.handlers.LatamPassErrorHandler;
import com.latam.pax.chloypremt.utils.Constants;
import com.siebel.customui.LOY37OCSLANQueryMemberLight;
import com.siebel.customui.OCSLANQueryMemberLightInput;
import com.siebel.customui.OCSLANQueryMemberLightOutput;
import com.siebel.customui.OCSSpcQuerySpcMemberSpcLightSpcWF;
import com.siebel.customui.SiebelFault;
import com.siebel.webservices.SiebelSessionType;
import com.siebel.xml.ocs_querymemberlight_input.ListOfLanInputQueryMemberLight;
import com.siebel.xml.ocs_querymemberlight_input.LoyMember;
import lombok.extern.slf4j.Slf4j;

/**
 * Query Member Light Client implementation class
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public class WSQueryMemberLightClient {

	private static final String PAGE_SIZE = "1";
	private static final String START_ROW_NUMBER = "0";
	private static final String ASTERISK = "*";

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	private static LOY37OCSLANQueryMemberLight service;

	public OCSLANQueryMemberLightOutput execute(ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {

		try {

			URL url = LOY37OCSLANQueryMemberLight.class
					.getResource(PROPERTIES.getProperty(Constants.WS_QUERY_MEMBER_LIGHT_WSDL));

			OCSSpcQuerySpcMemberSpcLightSpcWF port = getService(url).getOCSSpcQuerySpcMemberSpcLightSpcWF();

			((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					PROPERTIES.getProperty(Constants.WS_QUERY_MEMBER_LIGHT_ENDPOINT));

			OCSLANQueryMemberLightInput input = setInput(request);

			Holder<String> securityHolder = new Holder<>("");

			return port.ocslanQueryMemberLight(PROPERTIES.getProperty(Constants.WS_QUERY_MEMBER_LIGHT_USER),
					PROPERTIES.getProperty(Constants.WS_QUERY_MEMBER_LIGHT_PASS), SiebelSessionType.NONE,
					securityHolder, input);
			
		} catch (SOAPFaultException ex) {
			logger.error(ex.getMessage(), ex);

			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_LATAMPASS_EXECUTION_ID,
					PROPERTIES.getProperty(Constants.ERROR_LATAMPASS_EXECUTION), ex.getMessage());
		} catch (SiebelFault ex) {
			logger.error(ex.getMessage(), ex);

			LatamPassErrorHandler latamErrorHandler = new LatamPassErrorHandler(ex);
			throw new ChangeLoyaltyPremiumTicketsException(latamErrorHandler.getCode(), latamErrorHandler.getMessage(),
					latamErrorHandler.getNativeMessage());
		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);

			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_NOT_CATALOGED_ID,
					PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED), e.getMessage());
		}
		
	}

	public OCSLANQueryMemberLightInput setInput(ChangeLoyaltyPremiumTicketsRQDTO request) {
		OCSLANQueryMemberLightInput ocslanQueryMemberLightInput = new OCSLANQueryMemberLightInput();

		LoyMember loyaltymember = new LoyMember();
		loyaltymember.setMemberNumber(request.getLoyaltyMemberDTO().getFfNumber());
		loyaltymember.setProgramName(ASTERISK);

		ocslanQueryMemberLightInput.setListOfLanInputQueryMemberLight(new ListOfLanInputQueryMemberLight());
		ocslanQueryMemberLightInput.getListOfLanInputQueryMemberLight().getLoyMember().add(loyaltymember);
		ocslanQueryMemberLightInput.setPageSize(PAGE_SIZE);
		ocslanQueryMemberLightInput.setStartRowNum(START_ROW_NUMBER);

		return ocslanQueryMemberLightInput;
	}

	public LOY37OCSLANQueryMemberLight getService(URL url) throws ChangeLoyaltyPremiumTicketsException {
		synchronized (LOY37OCSLANQueryMemberLight.class) {
			if (service == null) {
				service = new LOY37OCSLANQueryMemberLight(url);
			}
			return service;
		}
	}

}
