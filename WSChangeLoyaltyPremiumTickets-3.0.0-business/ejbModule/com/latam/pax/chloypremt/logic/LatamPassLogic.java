package com.latam.pax.chloypremt.logic;

import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRSDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.utils.ClientUtil;
import com.latam.pax.chloypremt.utils.BusinessConverter;
import com.siebel.customui.ChangeExecutionOutput;

/**
 * LATAM PASS logic implementation
 * @author Alberto Tejos S.
 *
 */
public class LatamPassLogic {

	public ChangeLoyaltyPremiumTicketsRSDTO execute(ChangeLoyaltyPremiumTicketsRQDTO changeLoyaltyPremiumTicketsRQDTO)
			throws ChangeLoyaltyPremiumTicketsException {
		ChangeExecutionOutput changeExecutionOutput = ClientUtil
				.executeChangeExecution(changeLoyaltyPremiumTicketsRQDTO);
		return BusinessConverter.changeOutputToResponseDomain(changeExecutionOutput);
	}

}
