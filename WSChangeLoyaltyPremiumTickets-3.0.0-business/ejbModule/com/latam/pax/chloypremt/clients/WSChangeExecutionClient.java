package com.latam.pax.chloypremt.clients;

import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.soap.SOAPFaultException;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.domain.BookingDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.LoyaltyMemberDTO;
import com.latam.pax.chloypremt.domain.LoyaltyPremiumTicketsInfoDTO;
import com.latam.pax.chloypremt.domain.NewDTO;
import com.latam.pax.chloypremt.domain.OriginalDTO;
import com.latam.pax.chloypremt.domain.TicketDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.handlers.LatamPassErrorHandler;
import com.latam.pax.chloypremt.utils.Constants;
import com.latam.pax.chloypremt.utils.NumberUtil;
import com.latam.pax.chloypremt.utils.StringUtil;
import com.siebel.customui.ChangeExecutionInput;
import com.siebel.customui.ChangeExecutionOutput;
import com.siebel.customui.LOY37OCSLANQueryMemberLight;
import com.siebel.customui.LOY46SpcEjecutaSpcCambioSpcTicketSpcWS;
import com.siebel.customui.OCSSpcChangeSpcExecutionSpcWF;
import com.siebel.customui.SiebelFault;
import com.siebel.webservices.SiebelSessionType;
import com.siebel.xml.ocschangeexeinput.data.ListOfOcschangeexeinputData;
import com.siebel.xml.ocschangeexeinput.data.LoyMemberData;
import lombok.extern.slf4j.Slf4j;

/**
 * Change Execution Client implementation class
 * 
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public class WSChangeExecutionClient {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	private static LOY46SpcEjecutaSpcCambioSpcTicketSpcWS service;

	public ChangeExecutionOutput execute(final ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {

		try {

			URL url = LOY46SpcEjecutaSpcCambioSpcTicketSpcWS.class
					.getResource(PROPERTIES.getProperty(Constants.WS_CHANGE_EXECUTION_WSDL));

			OCSSpcChangeSpcExecutionSpcWF port = getService(url).getOCSSpcChangeSpcExecutionSpcWF();

			((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					PROPERTIES.getProperty(Constants.WS_CHANGE_EXECUTION_ENDPOINT));

			ChangeExecutionInput input = setInput(request);

			Holder<String> securityHolder = new Holder<>("");

			ChangeExecutionOutput changeExecutionOutput = port.changeExecution(input,
					PROPERTIES.getProperty(Constants.WS_CHANGE_EXECUTION_USER),
					PROPERTIES.getProperty(Constants.WS_CHANGE_EXECUTION_PASS), SiebelSessionType.NONE, securityHolder);

			if (null != changeExecutionOutput.getErrorSpcCode() && !changeExecutionOutput.getErrorSpcCode()
					.equals(String.valueOf(Constants.SUCCESSFUL_OPERATION_ID))) {

				throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_NOT_CATALOGED_ID,
						PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED),
						changeExecutionOutput.getErrorSpcMessage());
			}

			return changeExecutionOutput;

		} catch (SOAPFaultException ex) {
			logger.error(ex.getMessage(), ex);
			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_LATAMPASS_EXECUTION_ID,
					PROPERTIES.getProperty(Constants.ERROR_LATAMPASS_EXECUTION), ex.getMessage());

		} catch (SiebelFault ex) {
			logger.error(ex.getMessage(), ex);
			LatamPassErrorHandler latamErrorHandler = new LatamPassErrorHandler(ex);
			throw new ChangeLoyaltyPremiumTicketsException(latamErrorHandler.getCode(), latamErrorHandler.getMessage(),
					latamErrorHandler.getNativeMessage());

		} catch (ChangeLoyaltyPremiumTicketsException ex) {
			logger.error(ex.getMessage(), ex);
			throw new ChangeLoyaltyPremiumTicketsException(ex.getCode(), ex.getMessage(), ex.getNativeMessage());

		}

	}

	public ChangeExecutionInput setInput(final ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {

		ChangeExecutionInput changeExecutionInput = new ChangeExecutionInput();

		LoyaltyMemberDTO loyaltyMemberDTO = request.getLoyaltyMemberDTO();

		LoyaltyPremiumTicketsInfoDTO loyaltyPremiumTicketsInfoDTO = request.getLoyaltyPremiumTicketsInfoDTO();
		boolean isVoluntary = isVoluntary(loyaltyPremiumTicketsInfoDTO.getVoluntary());

		BookingDTO bookingDTO = loyaltyPremiumTicketsInfoDTO.getBookingDTO();

		ListOfOcschangeexeinputData listOfOcschangeexeinputData = new ListOfOcschangeexeinputData();

		List<TicketDTO> tickets = bookingDTO.getTickets();

		if (null != tickets && !tickets.isEmpty()) {
			for (TicketDTO ticketDTO : tickets) {
				LoyMemberData loyMemberData = new LoyMemberData();
				loyMemberData.setTicketNumber(ticketDTO.getTicketNumber());

				OriginalDTO originalDTO = ticketDTO.getOriginalDTO();
				NewDTO newDTO = ticketDTO.getNewDTO();

				String loyaltyCertificateNumber;

				if (originalDTO.getLoyaltyCertificateNumber().contains("-")) {
					loyaltyCertificateNumber = StringUtil.hyphenSplit(originalDTO.getLoyaltyCertificateNumber(), 1);
				} else {
					loyaltyCertificateNumber = originalDTO.getLoyaltyCertificateNumber();
				}

				loyMemberData.setCPOriginal(loyaltyCertificateNumber);

				String stringUsedPoints = originalDTO.getPointsDTO().getUsed();

				BigDecimal usedPoints = NumberUtil.stringToBigDecimal(stringUsedPoints);
				loyMemberData.setUsedPoints(usedPoints);

				String stringPricePoints = newDTO.getPricePoints();

				BigDecimal pricePoints = NumberUtil.stringToBigDecimal(stringPricePoints);
				loyMemberData.setNewTicketCost(pricePoints);

				loyMemberData.setProgramName(Constants.LATAMPASS);
				loyMemberData.setMemberNumber(loyaltyMemberDTO.getFfNumber());

				loyMemberData.setVoluntaryRefund(isVoluntary);

				String stringPenaltyPoints = loyaltyPremiumTicketsInfoDTO.getPenaltyPoints();

				BigDecimal penaltyPoints = NumberUtil.stringToBigDecimal(stringPenaltyPoints);
				loyMemberData.setPenaltyPoints(penaltyPoints);

				listOfOcschangeexeinputData.setLoyMember(loyMemberData);
			}
		}

		changeExecutionInput.setListOfOcschangeexeinput(listOfOcschangeexeinputData);

		return changeExecutionInput;
	}

	public LOY46SpcEjecutaSpcCambioSpcTicketSpcWS getService(final URL url)
			throws ChangeLoyaltyPremiumTicketsException {
		synchronized (LOY37OCSLANQueryMemberLight.class) {
			if (service == null) {
				service = new LOY46SpcEjecutaSpcCambioSpcTicketSpcWS(url);
			}
			return service;
		}
	}

	/**
	 * Check if indicador voluntario is true or false
	 * 
	 * @param voluntary
	 * @return boolean
	 */
	private boolean isVoluntary(String voluntary) {
		if (!StringUtil.isNullOrEmpty(voluntary)) {
			return Constants.VOLUNTARY.equalsIgnoreCase(voluntary) ? true : false;
		}

		return false;
	}

}
