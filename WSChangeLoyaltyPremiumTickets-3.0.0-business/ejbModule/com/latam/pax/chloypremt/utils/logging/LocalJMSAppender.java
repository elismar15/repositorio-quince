package com.latam.pax.chloypremt.utils.logging;

import java.net.UnknownHostException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.IllegalStateException;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.ErrorCode;
import org.apache.log4j.spi.LoggingEvent;

import lombok.Getter;
import lombok.Setter;


/**
 * This class must be used to add jms message on a local jms server.
 * By default this appender will be connected to LatamSplunkIntegrator resources (lsi),
 * but can be changed to any local resource
 * 
 * log4j.properties example:
 * <pre>
 * # JMS logger configuration.
 * log4j.appender.lsi=com.latam.arq.logging.LocalJMSAppender
 * log4j.appender.lsi.serviceName={appName}
 * log4j.appender.lsi.connectionFactoryBindingName=jms.factory.lsi
 * log4j.appender.lsi.bindingName=jms.queue.lsi
 * log4j.appender.lsi.layout=org.apache.log4j.PatternLayout
 * log4j.appender.lsi.layout.ConversionPattern=%d - %5p (%F:%L) - %m%n
 * </pre>
 * Properties description:
 * <ul>
 *   <li>serviceName: The service/application name to be added to the jms properties</li>
 *   <li>connectionFactoryBindingName: jndi name of the jms factory</li>
 *   <li>bindingName: Queue/Topic jndi name</li>
 *   <li>bindingType: Type of jms resoure</li>
 *   <li>expirationTime: Expiration time for the logged message (1 min by default)</li>
 *   <li>layout: Any log4j layout.</li>
 * </ul>
 *  
 * 
 * @author LATAM Enterprise Architect
 * @author cesar.pasache@latam.com (Cesar Pasache)
 *
 */
public class LocalJMSAppender extends AppenderSkeleton {
	//LogLog prefix
	private static final String PREFIX_LOG = LocalJMSAppender.class.getSimpleName() + " ";
	
	//Dummy logger
	private static final Logger logger = Logger.getLogger( LocalJMSAppender.class );
	
	private static enum BindingType { QUEUE,TOPIC };

	//Parameters
	@Getter @Setter
	private String connectionFactoryBindingName = "jms.factory.lsi";
	@Getter
	private String bindingType = "queue";
	@Getter @Setter
	private String bindingName = "jms.queue.lsi";	
	@Getter @Setter
	private String serviceName = null;
	private long expirationTime = 60000L;
	
	//Runtime variables
	protected Session session = null;
	protected Destination destination = null;
	protected MessageProducer producer = null;

	//Setting default values
	private static final String SERVER_NAME;	
	static{
		String serverName = "no-server-defined";
		try{
			serverName = java.net.InetAddress.getLocalHost().getHostName();
		}catch( final UnknownHostException e ){
			try{
				serverName = java.net.InetAddress.getLocalHost().getHostAddress();
			}catch( final UnknownHostException addressException){
				//no hostname detected
				logger.error( addressException );
			}
		}
		SERVER_NAME = serverName;
		
		//Disable the dummy logger to avoid deadlock's.
		logger.setLevel( Level.OFF );
	}			
	
	/**
	 * Initiate the jms connections
	 */
	public void activateOptions() {
		try {
			final InitialContext ctx = new InitialContext();
			final ConnectionFactory localConnectionFactory = (ConnectionFactory) ctx
					.lookup(getConnectionFactoryBindingName());
			final Connection localConnection = localConnectionFactory.createConnection();
			
			this.session = localConnection.createSession(false, 1);
			if (BindingType.TOPIC.toString().equalsIgnoreCase(getBindingType())) {
				this.destination = (Queue) ctx.lookup(getBindingName());
			} else {
				this.destination = (Topic) ctx.lookup(getBindingName());
			}
			this.producer = this.session.createProducer(this.destination);
			LogLog.debug( PREFIX_LOG + "Appender connected to " + getBindingName() );
			
			this.producer.setTimeToLive(this.expirationTime);
		} catch ( final NamingException | JMSException e ) {
			final String exceptionMessage = PREFIX_LOG + e.getMessage();
			LogLog.error( exceptionMessage );
			this.errorHandler.error( exceptionMessage, e, ErrorCode.FILE_OPEN_FAILURE );
			logger.error( exceptionMessage );
			close();
		}
	}

	public void close() {
		try {
			if (this.session != null) {
				this.session.close();
			}
		} catch ( final JMSException e ) {
			this.errorHandler.error( PREFIX_LOG + e.getMessage(), e, ErrorCode.CLOSE_FAILURE);
			logger.error(e);
		}
		this.session = null;
		this.destination = null;
		this.producer = null;
	}
	
	private void checkSession() throws LocalJMSAppenderException{
		if ((this.session == null) || (this.producer == null) || (this.destination == null)) {
			throw new LocalJMSAppenderException("JMSAppender not connected!!!");
		}		
	}
	
	private void checkDestination() throws LocalJMSAppenderException {
		try {
			this.producer.getDestination();
		} catch (IllegalStateException localIllegalStateException) {
			if (localIllegalStateException.getMessage().contains("closed")) {
				activateOptions();
			} else {
				throw new LocalJMSAppenderException("Can't open the jms destination resource.");
			}
		} catch (JMSException e){
			throw new LocalJMSAppenderException( e ); 
		}
	}
	
	private void sendMessage( final String messageContent ) throws LocalJMSAppenderException {
		try{
			final TextMessage msg = this.session.createTextMessage( messageContent );
			msg.setStringProperty("serviceName", getServiceName());
			msg.setStringProperty("serverName", SERVER_NAME);
			msg.setJMSExpiration(this.expirationTime);
			this.producer.send( msg );
		}catch(JMSException e){
			throw new LocalJMSAppenderException(e);
		}
	}

	protected void append( final LoggingEvent loggingEvent ) {
		try{
			checkSession();
			checkDestination();
			
			final StringBuilder messageContent = new StringBuilder();
			if (getLayout() != null) {
				messageContent.append(this.layout.format(loggingEvent));
			} else {
				messageContent.append(loggingEvent.getRenderedMessage());
			}
			if ((loggingEvent.getThrowableStrRep() != null)
					&& (loggingEvent.getThrowableStrRep().length > 0)) {
				for ( final String str : loggingEvent.getThrowableStrRep() ) {
					messageContent
						.append(str)
						.append("\n");
				}
			}
			sendMessage( messageContent.toString() );
			
		}catch( final LocalJMSAppenderException e ){
			LogLog.error(e.getMessage(), e);
			this.errorHandler.error(e.getMessage(), e, ErrorCode.WRITE_FAILURE);
			logger.error( e );
		}	
	}

	public boolean requiresLayout() {
		return true;
	}

	/**
	 * Setter override to ensure binding type  
	 * @param paramString
	 */
	public void setBindingType( final String paramString ) {
		if (BindingType.TOPIC.toString().equalsIgnoreCase(paramString)) {
			this.bindingType = "topic";
		}else{
			//default
			this.bindingType = "queue";
		}
	}

	public String getExpirationTime() {
		return String.valueOf(this.expirationTime);
	}
	public void setExpirationTime( final String paramString ) {
		try {
			this.expirationTime = Long.valueOf(paramString).longValue();
		} catch ( final NumberFormatException localNumberFormatException) {
			logger.warn("Can't parse expiration time.");
		}
	}
}
