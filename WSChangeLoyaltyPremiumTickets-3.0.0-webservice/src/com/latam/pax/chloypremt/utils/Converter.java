package com.latam.pax.chloypremt.utils;

import java.util.ArrayList;
import java.util.List;
import com.latam.entities.v3_0.ServiceStatusType;
import com.latam.entities.v3_1.LoyaltyMemberType;
import com.latam.entities.v3_1.PassengerBookingType;
import com.latam.entities.v3_1.PassengerBookingType.Documents.Document;
import com.latam.pax.chloypremt.domain.BookingDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRSDTO;
import com.latam.pax.chloypremt.domain.ChannelDTO;
import com.latam.pax.chloypremt.domain.ContextDTO;
import com.latam.pax.chloypremt.domain.DocumentDTO;
import com.latam.pax.chloypremt.domain.ExchangedDTO;
import com.latam.pax.chloypremt.domain.LoyaltyMemberDTO;
import com.latam.pax.chloypremt.domain.LoyaltyPremiumTicketDTO;
import com.latam.pax.chloypremt.domain.LoyaltyPremiumTicketsInfoDTO;
import com.latam.pax.chloypremt.domain.NewDTO;
import com.latam.pax.chloypremt.domain.OriginalDTO;
import com.latam.pax.chloypremt.domain.PassengerBookingTypeDTO;
import com.latam.pax.chloypremt.domain.PointsDTO;
import com.latam.pax.chloypremt.domain.ReAccreditedDTO;
import com.latam.pax.chloypremt.domain.ServiceStatusTypeDTO;
import com.latam.pax.chloypremt.domain.SessionContextDTO;
import com.latam.pax.chloypremt.domain.SourceDTO;
import com.latam.pax.chloypremt.domain.SystemDTO;
import com.latam.pax.chloypremt.domain.TicketDTO;
import com.latam.pax.chloypremt.domain.TransactionDTO;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.New;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original.Points;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Channel;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts.Context;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.Source;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.Exchanged;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.ReAccredited;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Transaction;

/**
 * Class that provides webservice layer convertion methods
 * 
 * @author Alberto Tejos S.
 *
 */
public final class Converter {

	private Converter() {
	}

	/**
	 * Convert service RQ object to domain RQ object
	 * 
	 * @param changeLoyaltyPremiumTicketsRQ
	 * @return ChangeLoyaltyPremiumTicketsRQDTO
	 */
	public static ChangeLoyaltyPremiumTicketsRQDTO serviceToRequestDomain(
			ChangeLoyaltyPremiumTicketsRQ changeLoyaltyPremiumTicketsRQ) {

		ChangeLoyaltyPremiumTicketsRQDTO changeLoyaltyPremiumTicketsRQDTO = new ChangeLoyaltyPremiumTicketsRQDTO();

		LoyaltyMemberDTO loyaltyMemberDTO = new LoyaltyMemberDTO();
		LoyaltyMemberType loyaltyMemberType = changeLoyaltyPremiumTicketsRQ.getLoyaltyMember();
		loyaltyMemberDTO.setFfNumber(loyaltyMemberType.getFfNumber());
		loyaltyMemberDTO.setProgramCode(loyaltyMemberType.getProgramCode());

		changeLoyaltyPremiumTicketsRQDTO.setLoyaltyMemberDTO(loyaltyMemberDTO);

		LoyaltyPremiumTicketsInfoDTO loyaltyPremiumTicketsInfoDTO = new LoyaltyPremiumTicketsInfoDTO();
		LoyaltyPremiumTicketsInfo loyaltyPremiumTicketsInfo = changeLoyaltyPremiumTicketsRQ
				.getLoyaltyPremiumTicketsInfo();

		BookingDTO bookingDTO = new BookingDTO();

		if (null != loyaltyPremiumTicketsInfo.getBooking()) {
			Booking booking = loyaltyPremiumTicketsInfo.getBooking();
			bookingDTO.setRecordLocator(booking.getRecordLocator());
			bookingDTO.setPointOfSale(booking.getPointOfSale());

			List<TicketDTO> ticketsDTO = new ArrayList<>();
			convertToDomainTickets(bookingDTO, booking, ticketsDTO);

		}

		loyaltyPremiumTicketsInfoDTO.setBookingDTO(bookingDTO);

		ChannelDTO channelDTO = new ChannelDTO();
		List<ContextDTO> contextsDTO = new ArrayList<>();

		if (null != loyaltyPremiumTicketsInfo.getChannel()) {
			Channel channel = loyaltyPremiumTicketsInfo.getChannel();
			channelDTO.setChannelName(channel.getChannelName());
			channelDTO.setSubChannelName(channel.getSubChannelName());
		}

		loyaltyPremiumTicketsInfoDTO.setChannelDTO(channelDTO);

		SessionContextDTO sessionContextDTO = new SessionContextDTO();
		SystemDTO systemDTO = new SystemDTO();

		if (null != loyaltyPremiumTicketsInfo.getSessionContext()) {
			SessionContext sessionContext = loyaltyPremiumTicketsInfo.getSessionContext();
			sessionContextDTO.setAuthorizationType(sessionContext.getAuthorizationType());

			List<Context> contexts = sessionContext.getContexts().getContext();

			for (Context context : contexts) {
				ContextDTO contextDTO = new ContextDTO();
				contextDTO.setKey(context.getKey());
				contextDTO.setValue(context.getValue());
				contextsDTO.add(contextDTO);
			}

			com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.System system = sessionContext
					.getSystem();
			systemDTO.setDeviceId(system.getDeviceId());
			systemDTO.setInterfaceId(system.getInterfaceId());
			systemDTO.setSystemId(system.getSystemId());

			sessionContextDTO.setIdSessionAuthorization(sessionContext.getIdSessionAuthorization());
			sessionContextDTO.setOriginTransactionId(sessionContext.getOriginTransactionId());
		}

		sessionContextDTO.setSystemDTO(systemDTO);
		sessionContextDTO.setContexts(contextsDTO);

		loyaltyPremiumTicketsInfoDTO.setSessionContextDTO(sessionContextDTO);

		loyaltyPremiumTicketsInfoDTO.setAuthorizationId(loyaltyPremiumTicketsInfo.getAuthorizationId());
		loyaltyPremiumTicketsInfoDTO.setPenaltyPoints(loyaltyPremiumTicketsInfo.getPenaltyPoints());
		loyaltyPremiumTicketsInfoDTO.setVoluntary(loyaltyPremiumTicketsInfo.getVoluntary());

		SourceDTO sourceDTO = new SourceDTO();

		if (null != changeLoyaltyPremiumTicketsRQ.getSource()) {
			Source source = changeLoyaltyPremiumTicketsRQ.getSource();
			sourceDTO.setIp(source.getIp());
			sourceDTO.setSystemId(source.getSystemId());
			sourceDTO.setTransactionDateTime(source.getTransactionDateTime());
			sourceDTO.setUserId(source.getUserId());
		}

		changeLoyaltyPremiumTicketsRQDTO.setSourceDTO(sourceDTO);

		changeLoyaltyPremiumTicketsRQDTO.setLoyaltyPremiumTicketsInfoDTO(loyaltyPremiumTicketsInfoDTO);

		return changeLoyaltyPremiumTicketsRQDTO;
	}

	/**
	 * Convert tickets to domain tickets
	 * 
	 * @param bookingDTO
	 * @param booking
	 * @param ticketsDTO
	 */
	private static void convertToDomainTickets(BookingDTO bookingDTO, Booking booking, List<TicketDTO> ticketsDTO) {
		if (!booking.getTickets().getTicket().isEmpty()) {
			for (Ticket ticket : booking.getTickets().getTicket()) {
				TicketDTO ticketDTO = new TicketDTO();

				OriginalDTO originalDTO = new OriginalDTO();
				Original original = ticket.getOriginal();
				originalDTO.setLoyaltyCertificateNumber(original.getLoyaltyCertificateNumber());

				Document document = new Document();
				DocumentDTO documentDTO = new DocumentDTO();
				documentDTO.setText(document.getText());
				documentDTO.setType(document.getType());

				PassengerBookingTypeDTO passengerBookingTypeDTO = new PassengerBookingTypeDTO();
				passengerBookingTypeDTO.setDocumentDTO(documentDTO);

				PassengerBookingType passengerBookingType = original.getPassenger();

				if (null != original.getPassenger()) {
					passengerBookingTypeDTO.setFirstName(passengerBookingType.getFirstName());
					passengerBookingTypeDTO.setLastName(passengerBookingType.getLastName());
					passengerBookingTypeDTO.setPassengerTypeCode(passengerBookingType.getPassengerTypeCode());
					passengerBookingTypeDTO.setPaxNameNumber(passengerBookingType.getPaxNameNumber());
				}

				originalDTO.setPassengerBookingTypeDTO(passengerBookingTypeDTO);

				PointsDTO pointsDTO = new PointsDTO();
				Points points = original.getPoints();
				pointsDTO.setExchanged(points.getExchanged());
				pointsDTO.setUsed(points.getUsed());

				originalDTO.setPointsDTO(pointsDTO);

				NewDTO newDTO = new NewDTO();
				New new1 = ticket.getNew();
				newDTO.setPassenger(passengerBookingTypeDTO);
				newDTO.setPricePoints(new1.getPricePoints());

				ticketDTO.setNewDTO(newDTO);
				ticketDTO.setOriginalDTO(originalDTO);
				ticketDTO.setTicketNumber(ticket.getTicketNumber());
				ticketsDTO.add(ticketDTO);
				bookingDTO.setTickets(ticketsDTO);
			}
		}
	}

	/**
	 * Convert domain service response to service response
	 * 
	 * @param changeLoyaltyPremiumTicketsRSDTO
	 * @return ChangeLoyaltyPremiumTicketsRS
	 */
	public static ChangeLoyaltyPremiumTicketsRS domainResponseToServiceResponse(
			ChangeLoyaltyPremiumTicketsRSDTO changeLoyaltyPremiumTicketsRSDTO) {

		LoyaltyPremiumTicketDTO loyaltyPremiumTicketDTO = changeLoyaltyPremiumTicketsRSDTO.getLoyaltyPremiumTicketDTO();
		LoyaltyMemberDTO loyaltyMemberDTO = loyaltyPremiumTicketDTO.getLoyaltyMemberDTO();
		LoyaltyMemberType loyaltyMemberType = new LoyaltyMemberType();

		if (null != loyaltyMemberDTO && null != loyaltyMemberDTO.getFfNumber()) {
			loyaltyMemberType.setFfNumber(loyaltyMemberDTO.getFfNumber());
		}

		if (null != loyaltyMemberDTO && null != loyaltyMemberDTO.getProgramCode()) {
			loyaltyMemberType.setProgramCode(loyaltyMemberDTO.getProgramCode());
		}

		List<TicketDTO> ticketsDTO = loyaltyPremiumTicketDTO.getTickets();
		Tickets tickets = new Tickets();

		setTicketsDomainToService(ticketsDTO, tickets);

		LoyaltyPremiumTicket loyaltyPremiumTicket = new LoyaltyPremiumTicket();
		loyaltyPremiumTicket.setLoyaltyMember(loyaltyMemberType);
		loyaltyPremiumTicket.setReceptId(loyaltyPremiumTicketDTO.getReceptId());
		loyaltyPremiumTicket.setTickets(tickets);

		ServiceStatusTypeDTO serviceStatusTypeDTO = changeLoyaltyPremiumTicketsRSDTO.getServiceStatusTypeDTO();

		ServiceStatusType serviceStatusType = new ServiceStatusType();
		serviceStatusType.setCode(serviceStatusTypeDTO.getCode());
		serviceStatusType.setMessage(serviceStatusTypeDTO.getMessage());
		serviceStatusType.setNativeMessage(serviceStatusTypeDTO.getMessage());

		ChangeLoyaltyPremiumTicketsRS changeLoyaltyPremiumTicketsRS = new ChangeLoyaltyPremiumTicketsRS();
		changeLoyaltyPremiumTicketsRS.setLoyaltyPremiumTicket(loyaltyPremiumTicket);
		changeLoyaltyPremiumTicketsRS.setServiceStatus(serviceStatusType);

		return changeLoyaltyPremiumTicketsRS;

	}

	/**
	 * Set domain tickets to service tickets
	 * 
	 * @param ticketsDTO
	 * @param tickets
	 */
	private static void setTicketsDomainToService(List<TicketDTO> ticketsDTO, Tickets tickets) {

		for (TicketDTO ticketDTO : ticketsDTO) {

			com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket ticket = new com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket();
			ticket.setTicketNumber(ticketDTO.getTicketNumber());

			com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New new1 = setServiceNewTicket(
					ticketDTO);

			com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original original = setServiceOriginalTicket(
					ticketDTO);

			ticket.setNew(new1);
			ticket.setOriginal(original);

			tickets.getTicket().add(ticket);

		}
	}

	/**
	 * Set domain original tickets to service original tickets
	 * 
	 * @param ticketDTO
	 * @return Original
	 */
	private static com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original setServiceOriginalTicket(
			TicketDTO ticketDTO) {

		OriginalDTO originalDTO = ticketDTO.getOriginalDTO();

		com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original original = setOriginal(
				originalDTO);

		PointsDTO pointsDTO = setOriginalPoints(originalDTO);

		Transaction transaction = setTransaction(originalDTO);

		Exchanged exchanged = new Exchanged();
		setExchanged(pointsDTO, exchanged);

		ReAccredited reAccredited = new ReAccredited();
		setReaccredited(pointsDTO, reAccredited);

		com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points points = null;

		points = setPoints(pointsDTO, exchanged, reAccredited, points);

		if (null != original) {
			original.setPoints(points);
			original.setTransaction(transaction);
		}

		return original;
	}

	/**
	 * Set domain original points to service original points
	 * 
	 * @param pointsDTO
	 * @param exchanged
	 * @param reAccredited
	 * @param points
	 * @return points
	 */
	private static com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points setPoints(
			PointsDTO pointsDTO, Exchanged exchanged, ReAccredited reAccredited,
			com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points points) {
		if (null != pointsDTO) {
			points = new com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points();
		}

		if (null != points) {
			points.setExchanged(exchanged);
			points.setReAccredited(reAccredited);
		}
		return points;
	}

	/**
	 * Set original domain to service original
	 * 
	 * @param originalDTO
	 * @return original
	 */
	private static com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original setOriginal(
			OriginalDTO originalDTO) {
		com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original original = null;

		if (null != originalDTO) {
			original = new com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original();
		}
		return original;
	}

	/**
	 * Set reaccredited
	 * 
	 * @param pointsDTO
	 * @param reAccredited
	 */
	private static void setReaccredited(PointsDTO pointsDTO, ReAccredited reAccredited) {
		ReAccreditedDTO reAccreditedDTO = new ReAccreditedDTO();

		if (null != pointsDTO && null != pointsDTO.getReAccreditedDTO()) {
			reAccreditedDTO = pointsDTO.getReAccreditedDTO();
		}

		if (null != pointsDTO && null != reAccreditedDTO && null != reAccreditedDTO.getAmount()) {
			reAccredited.setAmount(reAccreditedDTO.getAmount());
		}

		if (null != pointsDTO && null != reAccreditedDTO && null != reAccreditedDTO.getTransactionId()) {
			reAccredited.setTransactionId(reAccreditedDTO.getTransactionId());
		}
	}

	/**
	 * Set transaction domain to service transaction
	 * 
	 * @param originalDTO
	 * @param transaction
	 */
	private static Transaction setTransaction(OriginalDTO originalDTO) {
		TransactionDTO transactionDTO = null;
		Transaction transaction = null;

		if (null != originalDTO && null != originalDTO.getTransactionDTO()) {
			transactionDTO = originalDTO.getTransactionDTO();
			transaction = new Transaction();
		}

		if (null != transactionDTO && null != transactionDTO.getCancelId()) {
			transaction.setCancelId(transactionDTO.getCancelId());
		}

		if (null != transactionDTO && null != transactionDTO.getUsedId()) {
			transaction.setUsedId(transactionDTO.getUsedId());
		}

		return transaction;
	}

	/**
	 * Set exchanged domain to service exchanged
	 * 
	 * @param pointsDTO
	 * @param exchanged
	 */
	private static void setExchanged(PointsDTO pointsDTO, Exchanged exchanged) {
		ExchangedDTO exchangedDTO = new ExchangedDTO();

		if (null != pointsDTO && null != pointsDTO.getExchangedDTO()) {
			exchangedDTO = pointsDTO.getExchangedDTO();
		}

		if (null != pointsDTO && null != exchangedDTO && null != exchangedDTO.getAmount()) {
			exchanged.setAmount(exchangedDTO.getAmount());
		}

		if (null != pointsDTO && null != exchangedDTO && null != exchangedDTO.getTransactionId()) {
			exchanged.setTransactionId(exchangedDTO.getTransactionId());
		}
	}

	/**
	 * Set domain original points to service original points
	 * 
	 * @param originalDTO
	 * @return PointsDTO
	 */
	private static PointsDTO setOriginalPoints(OriginalDTO originalDTO) {
		PointsDTO pointsDTO = null;

		if (null != originalDTO && null != originalDTO.getPointsDTO()) {
			pointsDTO = originalDTO.getPointsDTO();
		}
		return pointsDTO;
	}

	/**
	 * Set domain new ticket to service new ticket
	 * 
	 * @param ticketDTO
	 * @return
	 */
	private static com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New setServiceNewTicket(
			TicketDTO ticketDTO) {
		NewDTO newDTO = ticketDTO.getNewDTO();
		com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New new1 = new com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New();

		if (null != newDTO && null != newDTO.getLoyaltyCertificateNumber()) {
			new1.setLoyaltyCertificateNumber(newDTO.getLoyaltyCertificateNumber());
		}

		if (null != newDTO && null != newDTO.getPaxNameNumber()) {
			new1.setPaxNameNumber(newDTO.getPaxNameNumber());
		}
		return new1;
	}

	/**
	 * Convert ServiceStatusTypeDTO to ServiceStatusType
	 * 
	 * @param changeLoyaltyPremiumTicketsRSDTO
	 * @return ServiceStatusType
	 */
	public static ServiceStatusType convertServiceStatus(
			ChangeLoyaltyPremiumTicketsRSDTO changeLoyaltyPremiumTicketsRSDTO) {
		ServiceStatusTypeDTO serviceStatusTypeDTO = changeLoyaltyPremiumTicketsRSDTO.getServiceStatusTypeDTO();
		final ServiceStatusType serviceStatusType = new ServiceStatusType();
		serviceStatusType.setCode(serviceStatusTypeDTO.getCode());
		serviceStatusType.setMessage(serviceStatusTypeDTO.getMessage());
		serviceStatusType.setNativeMessage(serviceStatusTypeDTO.getNativeMessage());
		return serviceStatusType;
	}

}
