package com.latam.pax.chloypremt.clients;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.LoyaltyMemberDTO;
import com.latam.pax.chloypremt.domain.LoyaltyPremiumTicketsInfoDTO;
import com.latam.pax.chloypremt.domain.SessionContextDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.handlers.LatamFidelidadeErrorHandler;
import com.latam.pax.chloypremt.handlers.WSSUsernameTokenSecurityHandler;
import com.latam.pax.chloypremt.utils.Constants;
import com.latam.pax.chloypremt.utils.DateUtil;
import com.latam.ws.multiplus.valsessaut.ErroTecnicoFaultMsg;
import com.latam.ws.multiplus.valsessaut.MetaInformacao;
import com.latam.ws.multiplus.valsessaut.Participante;
import com.latam.ws.multiplus.valsessaut.SessaoAutorizacao;
import com.latam.ws.multiplus.valsessaut.ValidarSessaoAutorizacaoParticipanteFaultMsg;
import com.latam.ws.multiplus.valsessaut.ValidarSessaoAutorizacaoParticipanteInput;
import com.latam.ws.multiplus.valsessaut.ValidarSessaoAutorizacaoParticipanteOutput;
import com.latam.ws.multiplus.valsessaut.ValidarSessaoAutorizacaoParticipantev1;
import com.latam.ws.multiplus.valsessaut.ValidarSessaoAutorizacaoParticipantev1PortType;
import lombok.extern.slf4j.Slf4j;

/**
 * Validar Sessao Autorizacao Participante Client implementation class
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public class WSValidarSessaoAutorizacaoParticipanteClient {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	private static ValidarSessaoAutorizacaoParticipantev1 service;

	public ValidarSessaoAutorizacaoParticipanteOutput execute(ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {
		ValidarSessaoAutorizacaoParticipanteOutput validarSessaoAutorizacaoParticipanteOutput = null;

		try {
			MetaInformacao metaInformacao = completeMetaInformacao();

			URL url = ValidarSessaoAutorizacaoParticipantev1.class
					.getResource(PROPERTIES.getProperty(Constants.WS_VALIDATE_SESSION_WSDL));

			ValidarSessaoAutorizacaoParticipantev1PortType port = getService(url)
					.getValidarSessaoAutorizacaoParticipantev1();

			((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					PROPERTIES.getProperty(Constants.WS_VALIDATE_SESSION_ENDPOINT));

			ValidarSessaoAutorizacaoParticipanteInput input = setInput(request);

			validarSessaoAutorizacaoParticipanteOutput = port.validarSessaoAutorizacaoParticipante(input,
					metaInformacao);

		} catch (ErroTecnicoFaultMsg | ValidarSessaoAutorizacaoParticipanteFaultMsg e) {
			logger.error(e.getMessage(), e);
			LatamFidelidadeErrorHandler errorHandler = new LatamFidelidadeErrorHandler(e);
			throw new ChangeLoyaltyPremiumTicketsException(errorHandler.getCode(), errorHandler.getMessage(),
					errorHandler.getNativeMessage());

		} catch (WebServiceException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_MULTIPLUS_SERVICE_ID,
					PROPERTIES.getProperty(Constants.ERROR_MULTIPLUS_SERVICE), e.getMessage());

		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_NOT_CATALOGED_ID,
					PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED), e.getMessage());

		}

		return validarSessaoAutorizacaoParticipanteOutput;
	}

	public ValidarSessaoAutorizacaoParticipanteInput setInput(ChangeLoyaltyPremiumTicketsRQDTO request) {
		ValidarSessaoAutorizacaoParticipanteInput validarSessaoAutorizacaoParticipanteInput = new ValidarSessaoAutorizacaoParticipanteInput();

		LoyaltyMemberDTO loyaltyMemberDTO = request.getLoyaltyMemberDTO();
		LoyaltyPremiumTicketsInfoDTO loyaltyPremiumTicketsInfoDTO = request.getLoyaltyPremiumTicketsInfoDTO();
		SessionContextDTO sessionContextDTO = loyaltyPremiumTicketsInfoDTO.getSessionContextDTO();

		Participante participante = new Participante();
		participante.setNumeroMultiplus(loyaltyMemberDTO.getFfNumber());

		SessaoAutorizacao sessaoAutorizacao = new SessaoAutorizacao();
		sessaoAutorizacao.setParticipante(participante);
		sessaoAutorizacao.setIdSessaoAutorizacao(sessionContextDTO.getIdSessionAuthorization());

		validarSessaoAutorizacaoParticipanteInput.setSessaoAutorizacao(sessaoAutorizacao);

		return validarSessaoAutorizacaoParticipanteInput;
	}

	public ValidarSessaoAutorizacaoParticipantev1 getService(URL url) {
		synchronized (WSValidarSessaoAutorizacaoParticipanteClient.class) {
			if (service == null) {
				service = new ValidarSessaoAutorizacaoParticipantev1(url);
				service.setHandlerResolver(new HandlerResolver() {
					@SuppressWarnings("rawtypes")
					@Override
					public List<Handler> getHandlerChain(PortInfo portInfo) {
						List<Handler> handlerList = new ArrayList<>();
						handlerList.add(new WSSUsernameTokenSecurityHandler(
								PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_USER),
								PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_PASS)));
						return handlerList;
					}
				});
			}
			return service;

		}
	}

	/**
	 * Complete MetaInformacao from service properties
	 * 
	 * @return MetaInformacao mapped
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	private MetaInformacao completeMetaInformacao() throws ChangeLoyaltyPremiumTicketsException {

		MetaInformacao metaInformacao = new MetaInformacao();
		metaInformacao.setIdRequisicao(PROPERTIES.getProperty(Constants.WS_META_INFO_ID_REQUIREMENT));
		metaInformacao.setDataHoraRequisicao(DateUtil.obtainXMLGregorianCalendarFromSysdate());
		metaInformacao.setSistemaOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_SYSTEM));
		metaInformacao.setOperacaoOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_OPERATION));
		metaInformacao.setEnderecoOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_ADDRESS));
		metaInformacao.setUsuarioOrigem(PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_USER));
		metaInformacao.setCanalOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_VERSION));
		metaInformacao.setVersaoConsumidor(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_TYPE));
		metaInformacao
				.setVersaoServicoConsumido(PROPERTIES.getProperty(Constants.WS_META_INFO_SERVICE_CONSUMED_VERSION));
		metaInformacao.setTipoConsumidor(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_TYPE));
		metaInformacao
				.setEnderecoServicoConsumido(PROPERTIES.getProperty(Constants.WS_META_INFO_SERVICE_CONSUMED_ADDRESS_VALID));

		return metaInformacao;
	}

}
