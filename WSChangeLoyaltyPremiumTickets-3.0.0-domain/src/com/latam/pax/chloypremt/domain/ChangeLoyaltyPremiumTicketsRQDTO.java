package com.latam.pax.chloypremt.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class ChangeLoyaltyPremiumTicketsRQDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	@NotNull(message="{}")
	private LoyaltyMemberDTO loyaltyMemberDTO;
	
	@Getter @Setter
	private LoyaltyPremiumTicketsInfoDTO loyaltyPremiumTicketsInfoDTO;
	
	@Getter @Setter
	private SourceDTO sourceDTO;
}
