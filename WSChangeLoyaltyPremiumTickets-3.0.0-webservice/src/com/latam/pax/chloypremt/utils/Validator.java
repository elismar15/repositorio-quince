package com.latam.pax.chloypremt.utils;

import java.util.Properties;

import javax.validation.ValidationException;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.arq.commons.validation.BeanValidator;
import com.latam.arq.commons.ws.exceptions.LATAMSoapException;
import com.latam.entities.v3_1.PassengerBookingType.Documents.Document;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket;
import lombok.extern.slf4j.Slf4j;

/**
 * Class that provides validation methods
 * 
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public final class Validator {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	private static final String LOY_MEMBER_DATA_NULL = "LoyMemberData: It can not be null";
	private static final String LOYALTY_PREMIUM_TICKET_INFO_NULL = "LoyaltyPremiumTicketsInfo: It can not be null";

	private Validator() {
	}

	/**
	 * Validates service request
	 * 
	 * @param request
	 * @throws LATAMSoapException
	 */
	public static void validateRequest(ChangeLoyaltyPremiumTicketsRQ request) throws LATAMSoapException {
		try {

			validateLoyMember(request);
			validaLoyaltyPremiumTicketInfo(request);

			BeanValidator.validateBean(request.getLoyaltyMember());
			BeanValidator.validateBean(request.getLoyaltyPremiumTicketsInfo());

			validateBooking(request);
			validateSource(request);
		} catch (ValidationException e) {
			logger.error(e.getMessage(), e);
			throw new LATAMSoapException(Constants.ERROR_INCORRECT_PARAMETERS_ID,
					PROPERTIES.getProperty(Constants.ERROR_INCORRECT_PARAMETERS) + e.getMessage());
		}
	}

	/**
	 * We validate the RQ LoyMember object to avoid validation when converting
	 * the RQ object to xml
	 * 
	 * @param request
	 * @throws ValidationException
	 */
	private static void validaLoyaltyPremiumTicketInfo(ChangeLoyaltyPremiumTicketsRQ request)
			throws ValidationException {
		if (null == request.getLoyaltyPremiumTicketsInfo()) {
			throw new ValidationException(LOYALTY_PREMIUM_TICKET_INFO_NULL);
		}
	}

	/**
	 * We validate the RQ LoyaltyPremiumTicketsInfo object to avoid validation
	 * when converting the RQ object to xml
	 * 
	 * @param request
	 * @throws ValidationException
	 */
	private static void validateLoyMember(ChangeLoyaltyPremiumTicketsRQ request) throws ValidationException {
		if (null == request.getLoyaltyMember()) {
			throw new ValidationException(LOY_MEMBER_DATA_NULL);
		}
	}

	/**
	 * Validates booking element, including all tickets inside
	 * 
	 * @param request
	 */
	private static void validateBooking(ChangeLoyaltyPremiumTicketsRQ request) {

		if (null != request.getLoyaltyPremiumTicketsInfo().getBooking()) {
			BeanValidator.validateBean(request.getLoyaltyPremiumTicketsInfo().getBooking());
			BeanValidator.validateBean(request.getLoyaltyPremiumTicketsInfo().getBooking().getTickets());
			validateTickets(request);
		}
	}

	/**
	 * Validates tickets, including originals and news
	 * 
	 * @param request
	 */
	private static void validateTickets(ChangeLoyaltyPremiumTicketsRQ request) {

		for (Ticket ticket : request.getLoyaltyPremiumTicketsInfo().getBooking().getTickets().getTicket()) {
			BeanValidator.validateBean(ticket);
			BeanValidator.validateBean(ticket.getOriginal());

			if (null != ticket.getOriginal().getPassenger()) {
				BeanValidator.validateBean(ticket.getOriginal().getPassenger());
			}

			validateOriginalPassengerDocuments(ticket);

			BeanValidator.validateBean(ticket.getOriginal().getPoints());
			BeanValidator.validateBean(ticket.getNew());

			validateNewPassenger(ticket);
		}

	}

	/**
	 * Validates new passenger
	 * 
	 * @param ticket
	 */
	private static void validateNewPassenger(Ticket ticket) {

		if (null != ticket.getNew().getPassenger()) {
			BeanValidator.validateBean(ticket.getNew().getPassenger());
			validateNewPassengerDocuments(ticket);
		}

	}

	/**
	 * Validates new passenger's documents
	 * 
	 * @param ticket
	 */
	private static void validateNewPassengerDocuments(Ticket ticket) {

		if (null != ticket.getNew().getPassenger().getDocuments()) {
			BeanValidator.validateBean(ticket.getNew().getPassenger());

			for (Document document : ticket.getNew().getPassenger().getDocuments().getDocument()) {
				BeanValidator.validateBean(document);
			}
		}

	}

	/**
	 * Validates original passenger's documents
	 * 
	 * @param ticket
	 */
	private static void validateOriginalPassengerDocuments(Ticket ticket) {

		if (null != ticket.getOriginal().getPassenger() && null != ticket.getOriginal().getPassenger().getDocuments()) {
			BeanValidator.validateBean(ticket.getOriginal().getPassenger().getDocuments());

			for (Document document : ticket.getOriginal().getPassenger().getDocuments().getDocument()) {
				BeanValidator.validateBean(document);
			}
		}

	}

	/**
	 * Validates source
	 * 
	 * @param request
	 */
	private static void validateSource(ChangeLoyaltyPremiumTicketsRQ request) {

		if (null != request.getSource()) {
			BeanValidator.validateBean(request.getSource());
		}

	}
}
