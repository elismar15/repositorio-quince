package com.latam.pax.chloypremt.utils;


/**
 * Constants class
 * @author Alberto Tejos S.
 *
 */
public final class Constants {

	public static final String VOLUNTARY	= "true";
	public static final String INVOLUNTARY 	= "false";
	
	public static final String LATAMPASS 		= "LATAM PASS";
	public static final String LATAM_FIDELIDADE = "LATAM FIDELIDADE";
	
	public static final int SUCCESSFUL_OPERATION_ID						=   0;
	public static final int ERROR_CHANGE_LOYALTY_PREMIUM_TICKET_ID		=  -1;
	public static final int ERROR_MULTIPLUS_SERVICE_ID					=  -2;
	public static final int ERROR_LATAMPASS_SERVICE_ID					=  -3;
	public static final int ERROR_VALIDATION_DENIED_ID					=  -4;
	public static final int ERROR_MULTIPLUS_EXECUTION_ID				=  -5;
	public static final int ERROR_LATAMPASS_EXECUTION_ID				=  -6;
	public static final int ERROR_INCORRECT_PARAMETERS_ID				=  -7;
	public static final int ERROR_INSUFFICIENT_BALANCE_ID				=  -8;
	public static final int ERROR_MEMBER_NOT_FOUND_ID					=  -9;
	public static final int ERROR_INCORRECT_INTERFACE_ID				=  -10;
	public static final int ERROR_AUTHENTICATION_FAILURE_ID				=  -11;
	public static final int ERROR_INVALID_AUTHENTICATION_CODE_ID		=  -12;
	public static final int ERROR_MEMBERSHIP_NUMBER_FORMAT_ID			=  -13;
	public static final int ERROR_AWARD_CODE_DOES_NOT_EXIST_ID			=  -18;
	public static final int ERROR_ACCOUNT_INACTIVE_ID					=  -20;
	public static final int ERROR_NOT_CATALOGED_ID						=  -33;
	
	public static final String RESPONSE_STATUS_OK 						= "ws.response.status.ok"; 
	public static final String RESPONSE_STATUS_NOK 						= "ws.response.status.nok"; 
	
	public static final String SERVICE_TYPE_LATAM_FIDELIDADE 			= "ws.service.type.latam.fidelidade";
	public static final String SERVICE_TYPE_LATAM_PASS					= "ws.service.type.latam.pass";
	
	public static final String SERVICE_NAME 							= "ws.service.name"; 
	
	public static final String SUCCESSFUL_OPERATION						= "message.correct.result";
	public static final String ERROR_CHANGE_LOYALTY_PREMIUM_TICKET		= "message.error.webservice";
	public static final String ERROR_MULTIPLUS_SERVICE					= "message.error.webservice.multiplus.available";
	public static final String ERROR_LATAMPASS_SERVICE					= "message.error.webservice.latampass.available";
	public static final String ERROR_VALIDATION_DENIED					= "message.error.validation.denied";
	public static final String ERROR_MULTIPLUS_EXECUTION				= "message.error.webservice.multiplus.execution";
	public static final String ERROR_LATAMPASS_EXECUTION				= "message.error.webservice.latampass.execution";
	public static final String ERROR_INCORRECT_PARAMETERS				= "message.error.incorrect.parameters";
	public static final String ERROR_INSUFFICIENT_BALANCE				= "message.error.insufficient.balance";
	public static final String ERROR_MEMBER_NOT_FOUND					= "message.error.member.not.found";
	public static final String ERROR_INCORRECT_INTERFACE				= "message.error.incorrect.interface";
	public static final String ERROR_AUTHENTICATION_FAILURE				= "message.error.authentication.failure";
	public static final String ERROR_INVALID_AUTHENTICATION_CODE		= "message.error.invalid.authentication.code";
	public static final String ERROR_MEMBERSHIP_NUMBER_FORMAT			= "message.error.membership.number.format";
	public static final String ERROR_AWARD_CODE_DOES_NOT_EXIST			= "message.error.award.code.does.not.exist";
	public static final String ERROR_ACCOUNT_INACTIVE					= "message.error.account.inactive";
	public static final String ERROR_INVALID_SYSTEM_IDENTIFIER 			= "message.error.invalid.system.identifier"; 
	public static final String ERROR_NOT_CATALOGED						= "message.error.not.cataloged";

	//MULTIPLUS clients
	
	//Criar SessaoAutorizacao Participante
	public static final String WS_CREATE_SESSION_WSDL 						= "ws.create.session.wsdl";
	public static final String WS_CREATE_SESSION_ENDPOINT 					= "ws.create.session.endpoint";
	public static final String WS_CREATE_SESSION_USER 						= "ws.create.session.user";
	public static final String WS_CREATE_SESSION_PASS 						= "ws.create.session.pass"; 
	
	//Metainfo
	public static final String WS_META_INFO_ID_REQUIREMENT					= "ws.metainfo.id.requisicao";
	public static final String WS_META_INFO_ORIGIN_SYSTEM 					= "ws.metainfo.origin.system";
	public static final String WS_META_INFO_ORIGIN_OPERATION				= "ws.metainfo.origin.operation";
	public static final String WS_META_INFO_ORIGIN_ADDRESS					= "ws.metainfo.origin.address";
	public static final String WS_META_INFO_ORIGIN_USER						= "ws.metainfo.origin.user";
	public static final String WS_META_INFO_CONSUMER_VERSION				= "ws.metainfo.consumer.version";
	public static final String WS_META_INFO_SERVICE_CONSUMED_VERSION 		= "ws.metainfo.consumed.service.version";
	public static final String WS_META_INFO_CONSUMER_TYPE 					= "ws.metainfo.consumer.type";
	public static final String WS_META_INFO_SERVICE_CONSUMED_ADDRESS_CRIAR	= "ws.metainfo.consumed.service.address.criar";
	public static final String WS_META_INFO_SERVICE_CONSUMED_ADDRESS_VALID	= "ws.metainfo.consumed.service.address.valid";
	public static final String WS_META_INFO_SERVICE_CONSUMED_ADDRESS_SUBST	= "ws.metainfo.consumed.service.address.subst";
	
	//Substituir Resgate Bilhete Aereo
	public static final String WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_WSDL		= "ws.substituir.resgate.bilhete.aereo.wsdl";
	public static final String WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_ENDPOINT	= "ws.substituir.resgate.bilhete.aereo.endpoint";
	public static final String WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_USER 	= "ws.substituir.resgate.bilhete.aereo.user";
	public static final String WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_PASS 	= "ws.substituir.resgate.bilhete.aereo.pass"; 
	
	//Validar Sessao Autorizacao Participante
	public static final String WS_VALIDATE_SESSION_WSDL						= "ws.validate.session.wsdl";
	public static final String WS_VALIDATE_SESSION_ENDPOINT					= "ws.validate.session.endpoint";
	public static final String WS_VALIDATE_SESSION_USER 					= "ws.validate.session.user";
	public static final String WS_VALIDATE_SESSION_PASS 					= "ws.validate.session.pass"; 
	
	//LATAM clients
	
	//LOY37 Query Member Light
	public static final String WS_QUERY_MEMBER_LIGHT_WSDL 					= "ws.query.member.light.wsdl";
	public static final String WS_QUERY_MEMBER_LIGHT_ENDPOINT 				= "ws.query.member.light.endpoint";
	public static final String WS_QUERY_MEMBER_LIGHT_USER 					= "ws.query.member.light.user";
	public static final String WS_QUERY_MEMBER_LIGHT_PASS 					= "ws.query.member.light.pass";
	
	//LOY46 Query Member Light
	public static final String WS_CHANGE_EXECUTION_WSDL 					= "ws.change.execution.wsdl";
	public static final String WS_CHANGE_EXECUTION_ENDPOINT 				= "ws.change.execution.endpoint";
	public static final String WS_CHANGE_EXECUTION_USER 					= "ws.change.execution.user";
	public static final String WS_CHANGE_EXECUTION_PASS 					= "ws.change.execution.pass"; 
	
	//MULTIPLUS errors
	public static final int MULTIPLUS_SERVICE_CODE_500000	= 500000;
	public static final int MULTIPLUS_SERVICE_CODE_503000 	= 503000;
	public static final int MULTIPLUS_SERVICE_CODE_503001 	= 503001;
	public static final int MULTIPLUS_SERVICE_CODE_422001 	= 422001;
	public static final int MULTIPLUS_SERVICE_CODE_404001 	= 404001;
	public static final int MULTIPLUS_SERVICE_CODE_422002 	= 422002;
	public static final int MULTIPLUS_SERVICE_CODE_401001 	= 401001;
	public static final int MULTIPLUS_SERVICE_CODE_404054 	= 404054;
	public static final int MULTIPLUS_SERVICE_CODE_404055 	= 404055;
	public static final int MULTIPLUS_SERVICE_CODE_409012	= 409012;
	
	//LATAM errors
	public static final String LATAM_SERVICE_CODE_RD002		= "RD002";
	public static final String LATAM_SERVICE_CODE_RD100		= "RD100";
	public static final String LATAM_SERVICE_CODE_RD101		= "RD101";
	public static final String LATAM_SERVICE_CODE_RD105		= "RD105";
	public static final String LATAM_SERVICE_CODE_RD106		= "RD106";
	
	private Constants(){}
	
}
