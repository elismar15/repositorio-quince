
package com.latam.ws.v3_0;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.validator.constraints.NotEmpty;

import com.latam.entities.v3_1.LoyaltyMemberType;
import com.latam.entities.v3_1.PassengerBookingType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LoyaltyMember" type="{http://entities.latam.com/v3_1}LoyaltyMemberType"/&gt;
 *         &lt;element name="LoyaltyPremiumTicketsInfo"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Booking" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="recordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="pointOfSale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Tickets"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Ticket" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Original"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType"/&gt;
 *                                                           &lt;element name="Points"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element name="exchanged" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                                     &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                           &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="New"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType" minOccurs="0"/&gt;
 *                                                           &lt;element name="pricePoints" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="penaltyPoints" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="voluntary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Channel" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="channelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="subChannelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="authorizationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SessionContext" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="authorizationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="originTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="System" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="interfaceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="deviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="Contexts" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Context" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                 &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="idSessionAuthorization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Source" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="transactionDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyMember",
    "loyaltyPremiumTicketsInfo",
    "source"
})
@XmlRootElement(name = "ChangeLoyaltyPremiumTicketsRQ")
public class ChangeLoyaltyPremiumTicketsRQ {
	
    @XmlElement(name = "LoyaltyMember", required = true)
    protected LoyaltyMemberType loyaltyMember;
    @XmlElement(name = "LoyaltyPremiumTicketsInfo", required = true)
    protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo loyaltyPremiumTicketsInfo;
    @XmlElement(name = "Source")
    protected ChangeLoyaltyPremiumTicketsRQ.Source source;

    /**
     * Obtiene el valor de la propiedad loyaltyMember.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyMemberType }
     *     
     */
    public LoyaltyMemberType getLoyaltyMember() {
        return loyaltyMember;
    }

    /**
     * Define el valor de la propiedad loyaltyMember.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyMemberType }
     *     
     */
    public void setLoyaltyMember(LoyaltyMemberType value) {
        this.loyaltyMember = value;
    }

    /**
     * Obtiene el valor de la propiedad loyaltyPremiumTicketsInfo.
     * 
     * @return
     *     possible object is
     *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo }
     *     
     */
    public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo getLoyaltyPremiumTicketsInfo() {
        return loyaltyPremiumTicketsInfo;
    }

    /**
     * Define el valor de la propiedad loyaltyPremiumTicketsInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo }
     *     
     */
    public void setLoyaltyPremiumTicketsInfo(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo value) {
        this.loyaltyPremiumTicketsInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad source.
     * 
     * @return
     *     possible object is
     *     {@link ChangeLoyaltyPremiumTicketsRQ.Source }
     *     
     */
    public ChangeLoyaltyPremiumTicketsRQ.Source getSource() {
        return source;
    }

    /**
     * Define el valor de la propiedad source.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeLoyaltyPremiumTicketsRQ.Source }
     *     
     */
    public void setSource(ChangeLoyaltyPremiumTicketsRQ.Source value) {
        this.source = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Booking" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="recordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="pointOfSale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Tickets"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Ticket" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Original"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType"/&gt;
     *                                                 &lt;element name="Points"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element name="exchanged" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                           &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                                 &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="New"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType" minOccurs="0"/&gt;
     *                                                 &lt;element name="pricePoints" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="penaltyPoints" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="voluntary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Channel" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="channelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="subChannelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="authorizationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SessionContext" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="authorizationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="originTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="System" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="interfaceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="deviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="Contexts" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Context" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                       &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="idSessionAuthorization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "booking",
        "penaltyPoints",
        "voluntary",
        "channel",
        "authorizationId",
        "sessionContext"
    })
    public static class LoyaltyPremiumTicketsInfo {

        @XmlElement(name = "Booking")
        protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking booking;
        protected String penaltyPoints;
        protected String voluntary;
        @XmlElement(name = "Channel")
        protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Channel channel;
        protected String authorizationId;
        @XmlElement(name = "SessionContext")
        protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext sessionContext;

        /**
         * Obtiene el valor de la propiedad booking.
         * 
         * @return
         *     possible object is
         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking }
         *     
         */
        public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking getBooking() {
            return booking;
        }

        /**
         * Define el valor de la propiedad booking.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking }
         *     
         */
        public void setBooking(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking value) {
            this.booking = value;
        }

        /**
         * Obtiene el valor de la propiedad penaltyPoints.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPenaltyPoints() {
            return penaltyPoints;
        }

        /**
         * Define el valor de la propiedad penaltyPoints.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPenaltyPoints(String value) {
            this.penaltyPoints = value;
        }

        /**
         * Obtiene el valor de la propiedad voluntary.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVoluntary() {
            return voluntary;
        }

        /**
         * Define el valor de la propiedad voluntary.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVoluntary(String value) {
            this.voluntary = value;
        }

        /**
         * Obtiene el valor de la propiedad channel.
         * 
         * @return
         *     possible object is
         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Channel }
         *     
         */
        public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Channel getChannel() {
            return channel;
        }

        /**
         * Define el valor de la propiedad channel.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Channel }
         *     
         */
        public void setChannel(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Channel value) {
            this.channel = value;
        }

        /**
         * Obtiene el valor de la propiedad authorizationId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthorizationId() {
            return authorizationId;
        }

        /**
         * Define el valor de la propiedad authorizationId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthorizationId(String value) {
            this.authorizationId = value;
        }

        /**
         * Obtiene el valor de la propiedad sessionContext.
         * 
         * @return
         *     possible object is
         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext }
         *     
         */
        public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext getSessionContext() {
            return sessionContext;
        }

        /**
         * Define el valor de la propiedad sessionContext.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext }
         *     
         */
        public void setSessionContext(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext value) {
            this.sessionContext = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="recordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="pointOfSale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Tickets"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Ticket" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Original"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType"/&gt;
         *                                       &lt;element name="Points"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element name="exchanged" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                                 &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                       &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="New"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType" minOccurs="0"/&gt;
         *                                       &lt;element name="pricePoints" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "recordLocator",
            "pointOfSale",
            "tickets"
        })
        public static class Booking {

            protected String recordLocator;
            protected String pointOfSale;
            @XmlElement(name = "Tickets", required = true)
            @NotNull(message="{rq.parameter.null}")
            protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets tickets;

            /**
             * Obtiene el valor de la propiedad recordLocator.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRecordLocator() {
                return recordLocator;
            }

            /**
             * Define el valor de la propiedad recordLocator.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRecordLocator(String value) {
                this.recordLocator = value;
            }

            /**
             * Obtiene el valor de la propiedad pointOfSale.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPointOfSale() {
                return pointOfSale;
            }

            /**
             * Define el valor de la propiedad pointOfSale.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPointOfSale(String value) {
                this.pointOfSale = value;
            }

            /**
             * Obtiene el valor de la propiedad tickets.
             * 
             * @return
             *     possible object is
             *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets }
             *     
             */
            public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets getTickets() {
                return tickets;
            }

            /**
             * Define el valor de la propiedad tickets.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets }
             *     
             */
            public void setTickets(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets value) {
                this.tickets = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Ticket" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Original"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType"/&gt;
             *                             &lt;element name="Points"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element name="exchanged" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                                       &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                             &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="New"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType" minOccurs="0"/&gt;
             *                             &lt;element name="pricePoints" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "ticket"
            })
            public static class Tickets {

                @XmlElement(name = "Ticket", required = true)
                @NotNull(message="{rq.parameter.null}")
                protected List<ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket> ticket;

                /**
                 * Gets the value of the ticket property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the ticket property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getTicket().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket }
                 * 
                 * 
                 */
                public List<ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket> getTicket() {
                    if (ticket == null) {
                        ticket = new ArrayList<>();
                    }
                    return this.ticket;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Original"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType"/&gt;
                 *                   &lt;element name="Points"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element name="exchanged" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                             &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                   &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="New"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType" minOccurs="0"/&gt;
                 *                   &lt;element name="pricePoints" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "ticketNumber",
                    "original",
                    "_new"
                })
                public static class Ticket {

                    @XmlElement(required = true)
                    @NotNull(message="{rq.parameter.null}")
                    @NotEmpty(message="{rq.parameter.empty}")
                    protected String ticketNumber;
                    @XmlElement(name = "Original", required = true)
                    @NotNull(message="{rq.parameter.null}")
                    protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original original;
                    @XmlElement(name = "New", required = true)
                    @NotNull(message="{rq.parameter.null}")
                    protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.New _new;

                    /**
                     * Obtiene el valor de la propiedad ticketNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTicketNumber() {
                        return ticketNumber;
                    }

                    /**
                     * Define el valor de la propiedad ticketNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTicketNumber(String value) {
                        this.ticketNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad original.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original }
                     *     
                     */
                    public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original getOriginal() {
                        return original;
                    }

                    /**
                     * Define el valor de la propiedad original.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original }
                     *     
                     */
                    public void setOriginal(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original value) {
                        this.original = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad new.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.New }
                     *     
                     */
                    public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.New getNew() {
                        return _new;
                    }

                    /**
                     * Define el valor de la propiedad new.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.New }
                     *     
                     */
                    public void setNew(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.New value) {
                        this._new = value;
                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType" minOccurs="0"/&gt;
                     *         &lt;element name="pricePoints" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passenger",
                        "pricePoints"
                    })
                    public static class New {

                        @XmlElement(name = "Passenger")
                        protected PassengerBookingType passenger;
                        @XmlElement(required = true)
                        @NotNull(message="{rq.parameter.null}")
                        @NotEmpty(message="{rq.parameter.empty}")
                        protected String pricePoints;

                        /**
                         * Obtiene el valor de la propiedad passenger.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PassengerBookingType }
                         *     
                         */
                        public PassengerBookingType getPassenger() {
                            return passenger;
                        }

                        /**
                         * Define el valor de la propiedad passenger.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PassengerBookingType }
                         *     
                         */
                        public void setPassenger(PassengerBookingType value) {
                            this.passenger = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad pricePoints.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPricePoints() {
                            return pricePoints;
                        }

                        /**
                         * Define el valor de la propiedad pricePoints.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPricePoints(String value) {
                            this.pricePoints = value;
                        }

                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Passenger" type="{http://entities.latam.com/v3_1}PassengerBookingType"/&gt;
                     *         &lt;element name="Points"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element name="exchanged" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *                   &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *         &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passenger",
                        "points",
                        "loyaltyCertificateNumber"
                    })
                    public static class Original {

                        @XmlElement(name = "Passenger", required = true)
                        protected PassengerBookingType passenger;
                        @XmlElement(name = "Points", required = true)
                        @NotNull(message="{rq.parameter.null}")
                        protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original.Points points;
                        @XmlElement(required = true)
                        @NotNull(message="{rq.parameter.null}")
                        protected String loyaltyCertificateNumber;

                        /**
                         * Obtiene el valor de la propiedad passenger.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PassengerBookingType }
                         *     
                         */
                        public PassengerBookingType getPassenger() {
                            return passenger;
                        }

                        /**
                         * Define el valor de la propiedad passenger.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PassengerBookingType }
                         *     
                         */
                        public void setPassenger(PassengerBookingType value) {
                            this.passenger = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad points.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original.Points }
                         *     
                         */
                        public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original.Points getPoints() {
                            return points;
                        }

                        /**
                         * Define el valor de la propiedad points.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original.Points }
                         *     
                         */
                        public void setPoints(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.Booking.Tickets.Ticket.Original.Points value) {
                            this.points = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad loyaltyCertificateNumber.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLoyaltyCertificateNumber() {
                            return loyaltyCertificateNumber;
                        }

                        /**
                         * Define el valor de la propiedad loyaltyCertificateNumber.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLoyaltyCertificateNumber(String value) {
                            this.loyaltyCertificateNumber = value;
                        }


                        /**
                         * <p>Clase Java para anonymous complex type.
                         * 
                         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element name="exchanged" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                         *         &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "exchanged",
                            "used"
                        })
                        public static class Points {

                            @XmlElement(required = true)
                            @NotNull(message="{rq.parameter.null}")
                            @NotEmpty(message="{rq.parameter.empty}")
                            protected String exchanged;
                            protected String used;

                            /**
                             * Obtiene el valor de la propiedad exchanged.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getExchanged() {
                                return exchanged;
                            }

                            /**
                             * Define el valor de la propiedad exchanged.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setExchanged(String value) {
                                this.exchanged = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad used.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getUsed() {
                                return used;
                            }

                            /**
                             * Define el valor de la propiedad used.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setUsed(String value) {
                                this.used = value;
                            }

                        }

                    }

                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="channelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="subChannelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "channelName",
            "subChannelName"
        })
        public static class Channel {

            protected String channelName;
            protected String subChannelName;

            /**
             * Obtiene el valor de la propiedad channelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelName() {
                return channelName;
            }

            /**
             * Define el valor de la propiedad channelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelName(String value) {
                this.channelName = value;
            }

            /**
             * Obtiene el valor de la propiedad subChannelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubChannelName() {
                return subChannelName;
            }

            /**
             * Define el valor de la propiedad subChannelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubChannelName(String value) {
                this.subChannelName = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="authorizationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="originTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="System" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="interfaceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="deviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="Contexts" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Context" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="idSessionAuthorization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "authorizationType",
            "originTransactionId",
            "system",
            "contexts",
            "idSessionAuthorization"
        })
        public static class SessionContext {

            protected String authorizationType;
            protected String originTransactionId;
            @XmlElement(name = "System")
            protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.System system;
            @XmlElement(name = "Contexts")
            protected ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts contexts;
            protected String idSessionAuthorization;

            /**
             * Obtiene el valor de la propiedad authorizationType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthorizationType() {
                return authorizationType;
            }

            /**
             * Define el valor de la propiedad authorizationType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthorizationType(String value) {
                this.authorizationType = value;
            }

            /**
             * Obtiene el valor de la propiedad originTransactionId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOriginTransactionId() {
                return originTransactionId;
            }

            /**
             * Define el valor de la propiedad originTransactionId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOriginTransactionId(String value) {
                this.originTransactionId = value;
            }

            /**
             * Obtiene el valor de la propiedad system.
             * 
             * @return
             *     possible object is
             *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.System }
             *     
             */
            public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.System getSystem() {
                return system;
            }

            /**
             * Define el valor de la propiedad system.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.System }
             *     
             */
            public void setSystem(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.System value) {
                this.system = value;
            }

            /**
             * Obtiene el valor de la propiedad contexts.
             * 
             * @return
             *     possible object is
             *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts }
             *     
             */
            public ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts getContexts() {
                return contexts;
            }

            /**
             * Define el valor de la propiedad contexts.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts }
             *     
             */
            public void setContexts(ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts value) {
                this.contexts = value;
            }

            /**
             * Obtiene el valor de la propiedad idSessionAuthorization.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdSessionAuthorization() {
                return idSessionAuthorization;
            }

            /**
             * Define el valor de la propiedad idSessionAuthorization.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdSessionAuthorization(String value) {
                this.idSessionAuthorization = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Context" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "context"
            })
            public static class Contexts {

                @XmlElement(name = "Context", required = true)
                protected List<ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts.Context> context;

                /**
                 * Gets the value of the context property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the context property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getContext().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts.Context }
                 * 
                 * 
                 */
                public List<ChangeLoyaltyPremiumTicketsRQ.LoyaltyPremiumTicketsInfo.SessionContext.Contexts.Context> getContext() {
                    if (context == null) {
                        context = new ArrayList<>();
                    }
                    return this.context;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "key",
                    "value"
                })
                public static class Context {

                    protected String key;
                    protected String value;

                    /**
                     * Obtiene el valor de la propiedad key.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getKey() {
                        return key;
                    }

                    /**
                     * Define el valor de la propiedad key.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setKey(String value) {
                        this.key = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad value.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setValue(String value) {
                        this.value = value;
                    }

                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="interfaceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="deviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "systemId",
                "interfaceId",
                "deviceId"
            })
            public static class System {

                protected String systemId;
                protected String interfaceId;
                protected String deviceId;

                /**
                 * Obtiene el valor de la propiedad systemId.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSystemId() {
                    return systemId;
                }

                /**
                 * Define el valor de la propiedad systemId.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSystemId(String value) {
                    this.systemId = value;
                }

                /**
                 * Obtiene el valor de la propiedad interfaceId.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getInterfaceId() {
                    return interfaceId;
                }

                /**
                 * Define el valor de la propiedad interfaceId.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setInterfaceId(String value) {
                    this.interfaceId = value;
                }

                /**
                 * Obtiene el valor de la propiedad deviceId.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDeviceId() {
                    return deviceId;
                }

                /**
                 * Define el valor de la propiedad deviceId.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDeviceId(String value) {
                    this.deviceId = value;
                }

            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="transactionDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "systemId",
        "userId",
        "transactionDateTime",
        "ip"
    })
    public static class Source {

        @XmlElement(required = true)
        @NotNull(message="{rq.parameter.null}")
        @NotEmpty(message="{rq.parameter.empty}")
        protected String systemId;
        protected String userId;
        protected String transactionDateTime;
        protected String ip;

        /**
         * Obtiene el valor de la propiedad systemId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSystemId() {
            return systemId;
        }

        /**
         * Define el valor de la propiedad systemId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSystemId(String value) {
            this.systemId = value;
        }

        /**
         * Obtiene el valor de la propiedad userId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserId() {
            return userId;
        }

        /**
         * Define el valor de la propiedad userId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserId(String value) {
            this.userId = value;
        }

        /**
         * Obtiene el valor de la propiedad transactionDateTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionDateTime() {
            return transactionDateTime;
        }

        /**
         * Define el valor de la propiedad transactionDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionDateTime(String value) {
            this.transactionDateTime = value;
        }

        /**
         * Obtiene el valor de la propiedad ip.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIp() {
            return ip;
        }

        /**
         * Define el valor de la propiedad ip.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIp(String value) {
            this.ip = value;
        }

    }

}
