package com.latam.pax.chloypremt.utils;

import java.util.Properties;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.clients.WSChangeExecutionClient;
import com.latam.pax.chloypremt.clients.WSCreateSessionClient;
import com.latam.pax.chloypremt.clients.WSQueryMemberLightClient;
import com.latam.pax.chloypremt.clients.WSSubstituirResgateBilheteAereoClient;
import com.latam.pax.chloypremt.clients.WSValidarSessaoAutorizacaoParticipanteClient;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.ws.multiplus.criarsessa.CriarSessaoAutorizacaoParticipanteOutput;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereoOutput;
import com.latam.ws.multiplus.valsessaut.ValidarSessaoAutorizacaoParticipanteOutput;
import com.siebel.customui.ChangeExecutionOutput;
import com.siebel.customui.OCSLANQueryMemberLightOutput;
import com.siebel.xml.ocs_querymemberlight_output.data.ListOfLanOutputQueryMemberLightData;
import lombok.extern.slf4j.Slf4j;

/**
 * Client utility class
 * 
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public final class ClientUtil {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	private ClientUtil() {
	}

	/**
	 * Execute Query Member Light client
	 * 
	 * @param request
	 * @return String program code
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static String executeQueryMemberLightClient(ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {

		WSQueryMemberLightClient wsQueryMemberLightClient = new WSQueryMemberLightClient();

		try {
			OCSLANQueryMemberLightOutput ocslanQueryMemberLightOutput = wsQueryMemberLightClient.execute(request);

			ListOfLanOutputQueryMemberLightData listOfLanOutputQueryMemberLightData = ocslanQueryMemberLightOutput
					.getListOfLanOutputQueryMemberLight();

			if (!listOfLanOutputQueryMemberLightData.getLoyMember().isEmpty()) {
				return listOfLanOutputQueryMemberLightData.getLoyMember().get(0).getProgramName();
			} else {
				throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_MEMBER_NOT_FOUND_ID,
						PROPERTIES.getProperty(Constants.ERROR_MEMBER_NOT_FOUND));
			}

		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getCode(), e.getMessage(), e.getNativeMessage());
		}
	}

	/**
	 * Execute Create Session client
	 * 
	 * @param request
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static CriarSessaoAutorizacaoParticipanteOutput executeCreateSessionClient(
			ChangeLoyaltyPremiumTicketsRQDTO request) throws ChangeLoyaltyPremiumTicketsException {

		WSCreateSessionClient wsCreateSessionClient = new WSCreateSessionClient();
		try {
			return wsCreateSessionClient.execute(request);
		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getCode(), e.getMessage(), e.getNativeMessage());
		}
	}

	/**
	 * Execute Validate Session client
	 * 
	 * @param request
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static ValidarSessaoAutorizacaoParticipanteOutput executeValidateSessionClient(
			ChangeLoyaltyPremiumTicketsRQDTO request) throws ChangeLoyaltyPremiumTicketsException {

		WSValidarSessaoAutorizacaoParticipanteClient wsValidarSessaoAutorizacaoParticipanteClient = new WSValidarSessaoAutorizacaoParticipanteClient();

		try {
			return wsValidarSessaoAutorizacaoParticipanteClient.execute(request);
		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getCode(), e.getMessage(), e.getNativeMessage());
		}
	}

	/**
	 * Execute Change Execution client
	 * 
	 * @param request
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static ChangeExecutionOutput executeChangeExecution(ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {

		WSChangeExecutionClient wsChangeExecutionClient = new WSChangeExecutionClient();

		try {
			return wsChangeExecutionClient.execute(request);
		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getCode(), e.getMessage(), e.getNativeMessage());
		}
	}

	/**
	 * Execute Substituir Resgate Bilhete Aereo client
	 * 
	 * @param request
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static SubstituirResgateBilheteAereoOutput executeSubstituirResgateBilheteAereoClient(
			ChangeLoyaltyPremiumTicketsRQDTO request) throws ChangeLoyaltyPremiumTicketsException {

		WSSubstituirResgateBilheteAereoClient wsSubstituirResgateBilheteAereoClient = new WSSubstituirResgateBilheteAereoClient();

		try {
			return wsSubstituirResgateBilheteAereoClient.execute(request);
		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getCode(), e.getMessage(), e.getNativeMessage());
		}
	}

}
