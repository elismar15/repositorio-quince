package com.latam.pax.chloypremt.utils.logging;

/**
 * Exception class for LocalJMSAppender
 * (protected class).
 * 
 * @author LATAM Enterprise Architect
 * @author cesar.pasache@latam.com (Cesar Pasache)
 *
 */
class LocalJMSAppenderException extends Exception {
	private static final long serialVersionUID = 4508942450592682815L;

	public LocalJMSAppenderException() {
		super();
	}

	public LocalJMSAppenderException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public LocalJMSAppenderException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public LocalJMSAppenderException(String arg0) {
		super(arg0);
	}

	public LocalJMSAppenderException(Throwable cause) {
		super(cause);
	}
	
}
