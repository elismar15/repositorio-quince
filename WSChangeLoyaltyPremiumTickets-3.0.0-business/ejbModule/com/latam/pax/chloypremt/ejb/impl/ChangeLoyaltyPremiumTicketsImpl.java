package com.latam.pax.chloypremt.ejb.impl;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRSDTO;
import com.latam.pax.chloypremt.ejb.services.ChangeLoyaltyPremiumTicketsServices;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.logic.LatamFidelidadeLogic;
import com.latam.pax.chloypremt.logic.LatamPassLogic;
import com.latam.pax.chloypremt.utils.ClientUtil;
import com.latam.pax.chloypremt.utils.Constants;
import lombok.extern.slf4j.Slf4j;

/**
 * EJB implementation of ChangeLoyaltyPremiumTickets-3.0.0
 * 
 * @author ANT using import-ws target
 * @author LATAM Enterprise Architect
 * @author Alberto Tejos S.
 */
@Stateless(name = "ChangeLoyaltyPremiumTicketsEJB", description = "Business logic implementation for ChangeLoyaltyPremiumTickets service")
@Slf4j
@Interceptors({ChangeLoyaltyPremiumTicketsImpl.class})
public class ChangeLoyaltyPremiumTicketsImpl implements ChangeLoyaltyPremiumTicketsServices {

	@Override
	@Interceptors({ChangeLoyaltyPremiumTicketsImpl.class})
	public ChangeLoyaltyPremiumTicketsRSDTO execute(ChangeLoyaltyPremiumTicketsRQDTO changeLoyaltyPremiumTicketsRQDTO)
			throws ChangeLoyaltyPremiumTicketsException {

		ChangeLoyaltyPremiumTicketsRSDTO changeLoyaltyPremiumTicketsRSDTO = null;

		try {

			String programCode = changeLoyaltyPremiumTicketsRQDTO.getLoyaltyMemberDTO().getProgramCode();
			programCode = (null == programCode || programCode.isEmpty()) ? ClientUtil.executeQueryMemberLightClient(changeLoyaltyPremiumTicketsRQDTO) 
																		 : programCode;

			if (programCode.equals(Constants.LATAMPASS)) {
				LatamPassLogic latamPassLogic = new LatamPassLogic();
				return latamPassLogic.execute(changeLoyaltyPremiumTicketsRQDTO);
			} else if (programCode.equals(Constants.LATAM_FIDELIDADE)) {
				LatamFidelidadeLogic latamFidelidadeLogic = new LatamFidelidadeLogic();
				return latamFidelidadeLogic.execute(changeLoyaltyPremiumTicketsRQDTO);
			}
		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getCode(), e.getMessage(), e.getNativeMessage());
		}

		return changeLoyaltyPremiumTicketsRSDTO;
	}

}
