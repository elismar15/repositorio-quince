package com.latam.pax.chloypremt.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class PointsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	@NotNull(message="{}")
	private String exchanged;
	
	@Getter @Setter
	private String used;
	
	@Getter @Setter
	private ExchangedDTO exchangedDTO;
	
	@Getter @Setter
	private ReAccreditedDTO reAccreditedDTO;
	
}
