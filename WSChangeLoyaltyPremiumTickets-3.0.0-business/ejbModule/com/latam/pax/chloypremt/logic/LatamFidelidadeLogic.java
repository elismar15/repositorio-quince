package com.latam.pax.chloypremt.logic;

import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRSDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.utils.ClientUtil;
import com.latam.pax.chloypremt.utils.BusinessConverter;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereoOutput;

/**
 * LATAM FIDELIDADE logic implementation
 * 
 * @author Alberto Tejos S.
 *
 */
public class LatamFidelidadeLogic {

	public ChangeLoyaltyPremiumTicketsRSDTO execute(ChangeLoyaltyPremiumTicketsRQDTO changeLoyaltyPremiumTicketsRQDTO)
			throws ChangeLoyaltyPremiumTicketsException {

		SubstituirResgateBilheteAereoOutput substituirOutput = ClientUtil
				.executeSubstituirResgateBilheteAereoClient(changeLoyaltyPremiumTicketsRQDTO);
		return BusinessConverter.substituirOutputToResponseDomain(substituirOutput);

	}

}
