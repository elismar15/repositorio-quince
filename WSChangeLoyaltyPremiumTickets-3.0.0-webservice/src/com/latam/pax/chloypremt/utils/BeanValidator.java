package com.latam.pax.chloypremt.utils;

import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Utility to validate a bean using JSR303 standards on any bean
 * 
 * @author LATAM Enterprise Architect
 *
 */
public final class BeanValidator {	
	private static final ValidatorFactory FACTORY = Validation.buildDefaultValidatorFactory();
	private static final String RAWTYPES = "rawtypes";
	
	private BeanValidator(){
		
	}
	
	/**
	 * Validate any bean using the JSR303 standards defined on the bean source.
	 * 
	 * @param object
	 * @throws ValidationException
	 */
	@SuppressWarnings(RAWTYPES)
	public static void validateBean( final Object object ) throws ValidationException{
		final Validator validator = FACTORY.getValidator();
		final Set violations = validator.validate( object );
		if (!violations.isEmpty()){
			final String exceptionMessage = getExceptionMessage( violations );
			throw new ValidationException( exceptionMessage );
		}
	}
	
	@SuppressWarnings(RAWTYPES)
	private static String getExceptionMessage( final Set violations ){
		final StringBuilder msg = new StringBuilder();
		final Iterator iterator = violations.iterator();
		while(iterator.hasNext()){
			final Object violation = iterator.next();
			if ((violation!=null) && (violation instanceof ConstraintViolation)){
				appendMessage( msg, (ConstraintViolation) violation );
			}
		}
		return msg.toString();
	}
	
	@SuppressWarnings(RAWTYPES)
	private static void appendMessage( final StringBuilder msg, final ConstraintViolation constraintViolation ){
		if (msg.length()>0){
			msg.append(" ; ");
		}
		msg
			.append( getPropertyName(constraintViolation) )
			.append( ": ")
			.append( constraintViolation.getMessage() );
	}
	
	@SuppressWarnings(RAWTYPES)
	private static String getPropertyName( final ConstraintViolation constraintViolation ){
		final Iterator iterator = constraintViolation.getPropertyPath().iterator();
		while(iterator.hasNext()){
			final Object value = iterator.next();
			if ((value!=null) && (!iterator.hasNext())){
				return value.toString();
			}
		}
		return "";
	}
	
}
