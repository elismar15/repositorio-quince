package com.latam.pax.chloypremt.utils;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Properties;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
/**
 * Utility class for service logs
 * @author Alberto Tejos S.
 *
 */
public final class LogUtil {
	
	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();
	private static final String LOG_SEPARATOR = ";";
	
	public static final SimpleDateFormat LOG_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
	
	private LogUtil(){}
	
	/**
	 * Creates log service
	 * @param startDate
	 * @param timeMilis
	 * @param serviceStatus
	 * @param appName
	 * @param channel
	 * @param errorId
	 * @param xmlObject
	 * @param nativeMessage
	 * @return
	 */
	public static String createLog(String startDate, long timeMilis, String serviceStatus, String appName, String channel,
			int errorId, String xmlObject, String nativeMessage, String serviceType) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder
			.append(LOG_SEPARATOR)
			.append(startDate)
			.append(LOG_SEPARATOR)
			.append(timeMilis)
			.append(LOG_SEPARATOR)
			.append(serviceType)
			.append(LOG_SEPARATOR)
			.append(PROPERTIES.getProperty(Constants.SERVICE_NAME))
			.append(LOG_SEPARATOR)
			.append(serviceStatus)
			.append(LOG_SEPARATOR)
			.append(appName)
			.append(LOG_SEPARATOR)
			.append(channel)
			.append(LOG_SEPARATOR)
			.append(errorId)
			.append(LOG_SEPARATOR)
			.append(xmlObject)
			.append(LOG_SEPARATOR)
			.append(nativeMessage);

		return stringBuilder.toString();

	}
	
	/**
	 * Object to XML.
	 *
	 * @param object the object
	 * @return the string
	 */
	public static String objectToXML(Object object) {
		 String xmlString = "";
		try {
			final JAXBContext context = JAXBContext.newInstance(object.getClass());
			final Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			final StringWriter salida = new StringWriter();
			m.marshal(object, salida);
			xmlString = salida.toString();
		} catch (JAXBException e) {
			logger.error(e.getMessage(),e);
		}
		return xmlString;
	}
	
}
