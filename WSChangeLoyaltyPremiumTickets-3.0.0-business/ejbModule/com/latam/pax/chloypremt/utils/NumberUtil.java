package com.latam.pax.chloypremt.utils;

import java.math.BigDecimal;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import lombok.extern.slf4j.Slf4j;

/**
 * Numbers utility class
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public final class NumberUtil {

	private NumberUtil() {

	}

	/**
	 * Convert String to BigDecimal
	 * @param string
	 * @return BigDecimal object
	 */
	public static BigDecimal stringToBigDecimal(final String string) throws ChangeLoyaltyPremiumTicketsException {
		if (!StringUtil.isNullOrEmpty(string)) {
			try {
				return new BigDecimal(string);
			} catch (NumberFormatException e) {
				logger.error(e.getMessage(), e);
				throw new ChangeLoyaltyPremiumTicketsException(e.getMessage());
			}
		}
		return null;
		
	}
}
