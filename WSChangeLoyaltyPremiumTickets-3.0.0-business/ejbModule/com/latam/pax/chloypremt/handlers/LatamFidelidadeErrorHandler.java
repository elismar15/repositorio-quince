package com.latam.pax.chloypremt.handlers;

import java.util.HashMap;
import java.util.Properties;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.utils.Constants;
import com.latam.ws.multiplus.criarsessa.CriarSessaoAutorizacaoParticipanteFaultMsg;
import com.latam.ws.multiplus.criarsessa.ErroTecnicoFaultMsg;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereoFaultMsg;
import com.latam.ws.multiplus.valsessaut.ValidarSessaoAutorizacaoParticipanteFaultMsg;

/**
 * Class to handle multiplus errors
 * 
 * @author Alberto Tejos
 */
public final class LatamFidelidadeErrorHandler extends ErrorHandler {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	/**
	 * Constructor
	 * 
	 * @param exception
	 */
	public LatamFidelidadeErrorHandler(Exception exception) {
		super(exception);

		fillServiceErrorMap();

		ServiceError multiplusError = new ServiceError();

		if (exception instanceof CriarSessaoAutorizacaoParticipanteFaultMsg) {

			multiplusError = getServiceError(Integer
					.parseInt(((CriarSessaoAutorizacaoParticipanteFaultMsg) exception).getFaultInfo().getCodigo()));

		} else if (exception instanceof SubstituirResgateBilheteAereoFaultMsg) {

			multiplusError = getServiceError(
					Integer.parseInt(((SubstituirResgateBilheteAereoFaultMsg) exception).getFaultInfo().getCodigo()));

		} else if (exception instanceof ValidarSessaoAutorizacaoParticipanteFaultMsg) {

			multiplusError = getServiceError(Integer
					.parseInt(((ValidarSessaoAutorizacaoParticipanteFaultMsg) exception).getFaultInfo().getCodigo()));

		}

		this.code = multiplusError.getErrorCode();
		this.message = multiplusError.getErrorMessage();
		this.nativeMessage = appendNativeMessage();
	}

	/**
	 * Constructor
	 * 
	 * @param erroTecnicoFaultMsg
	 */
	public LatamFidelidadeErrorHandler(ErroTecnicoFaultMsg erroTecnicoFaultMsg) {
		super(erroTecnicoFaultMsg);

		fillServiceErrorMap();

		ServiceError multiplusError = getServiceError(Integer.parseInt(erroTecnicoFaultMsg.getFaultInfo().getCodigo()));

		this.code = multiplusError.getErrorCode();
		this.message = multiplusError.getErrorMessage();
		this.nativeMessage = appendNativeMessage();

	}

	/**
	 * Constructor
	 * 
	 * @param erroTecnicoFaultMsg
	 */
	public LatamFidelidadeErrorHandler(com.latam.ws.multiplus.subsresbil.ErroTecnicoFaultMsg erroTecnicoFaultMsg) {
		super(erroTecnicoFaultMsg);

		fillServiceErrorMap();

		ServiceError multiplusError = getServiceError(Integer.parseInt(erroTecnicoFaultMsg.getFaultInfo().getCodigo()));

		this.code = multiplusError.getErrorCode();
		this.message = multiplusError.getErrorMessage();
		this.nativeMessage = appendNativeMessage();

	}

	/**
	 * Constructor
	 * 
	 * @param erroTecnicoFaultMsg
	 */
	public LatamFidelidadeErrorHandler(com.latam.ws.multiplus.valsessaut.ErroTecnicoFaultMsg erroTecnicoFaultMsg) {
		super(erroTecnicoFaultMsg);

		fillServiceErrorMap();

		ServiceError multiplusError = getServiceError(Integer.parseInt(erroTecnicoFaultMsg.getFaultInfo().getCodigo()));

		this.code = multiplusError.getErrorCode();
		this.message = multiplusError.getErrorMessage();
		this.nativeMessage = appendNativeMessage();

	}

	@Override
	protected String appendNativeMessage() {

		StringBuilder builder = new StringBuilder();

		if (exception instanceof ErroTecnicoFaultMsg) {

			ErroTecnicoFaultMsg erroTecnicoFaultMsg = (ErroTecnicoFaultMsg) exception;

			builder.append(erroTecnicoFaultMsg.getFaultInfo().getCodigo()).append(BLANK)
					.append(printError(erroTecnicoFaultMsg.getFaultInfo().getMensagem())).append(BLANK)
					.append(printError(erroTecnicoFaultMsg.getFaultInfo().getInstrucao())).append(BLANK)
					.append(printError(erroTecnicoFaultMsg.getFaultInfo().getDetalhe()));

		} else if (exception instanceof CriarSessaoAutorizacaoParticipanteFaultMsg) {

			CriarSessaoAutorizacaoParticipanteFaultMsg criarSessaoAutorizacaoParticipanteFaultMsg = (CriarSessaoAutorizacaoParticipanteFaultMsg) exception;

			builder.append(criarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getCodigo()).append(BLANK)
					.append(printError(criarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getMensagem()))
					.append(BLANK)
					.append(printError(criarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getInstrucao()))
					.append(BLANK)
					.append(printError(criarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getDetalhe()));

		} else if (exception instanceof SubstituirResgateBilheteAereoFaultMsg) {

			SubstituirResgateBilheteAereoFaultMsg substituirResgateBilheteAereoFaultMsg = (SubstituirResgateBilheteAereoFaultMsg) exception;

			builder.append(substituirResgateBilheteAereoFaultMsg.getFaultInfo().getCodigo()).append(BLANK)
					.append(printError(substituirResgateBilheteAereoFaultMsg.getFaultInfo().getMensagem()))
					.append(BLANK)
					.append(printError(substituirResgateBilheteAereoFaultMsg.getFaultInfo().getInstrucao()))
					.append(BLANK)
					.append(printError(substituirResgateBilheteAereoFaultMsg.getFaultInfo().getDetalhe()));

		} else if (exception instanceof ValidarSessaoAutorizacaoParticipanteFaultMsg) {

			ValidarSessaoAutorizacaoParticipanteFaultMsg validarSessaoAutorizacaoParticipanteFaultMsg = (ValidarSessaoAutorizacaoParticipanteFaultMsg) exception;

			builder.append(validarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getCodigo()).append(BLANK)
					.append(printError(validarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getMensagem()))
					.append(BLANK)
					.append(printError(validarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getInstrucao()))
					.append(BLANK)
					.append(printError(validarSessaoAutorizacaoParticipanteFaultMsg.getFaultInfo().getDetalhe()));

		}

		return builder.toString();

	}

	@Override
	protected void fillServiceErrorMap() {

		serviceErrorMap = new HashMap<>();

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_500000, new ServiceError(
				Constants.ERROR_MULTIPLUS_EXECUTION_ID, PROPERTIES.getProperty(Constants.ERROR_MULTIPLUS_EXECUTION)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_503000, new ServiceError(
				Constants.ERROR_MULTIPLUS_EXECUTION_ID, PROPERTIES.getProperty(Constants.ERROR_MULTIPLUS_EXECUTION)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_503001, new ServiceError(
				Constants.ERROR_MULTIPLUS_EXECUTION_ID, PROPERTIES.getProperty(Constants.ERROR_MULTIPLUS_EXECUTION)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_422001, new ServiceError(
				Constants.ERROR_INCORRECT_PARAMETERS_ID, PROPERTIES.getProperty(Constants.ERROR_INCORRECT_PARAMETERS)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_422002, new ServiceError(
				Constants.ERROR_INCORRECT_PARAMETERS_ID, PROPERTIES.getProperty(Constants.ERROR_INCORRECT_INTERFACE)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_401001,
				new ServiceError(Constants.ERROR_AUTHENTICATION_FAILURE_ID,
						PROPERTIES.getProperty(Constants.ERROR_AUTHENTICATION_FAILURE)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_404054,
				new ServiceError(Constants.ERROR_AUTHENTICATION_FAILURE_ID,
						PROPERTIES.getProperty(Constants.ERROR_INVALID_SYSTEM_IDENTIFIER)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_404055, new ServiceError(Constants.ERROR_NOT_CATALOGED_ID,
				PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_404001, new ServiceError(
				Constants.ERROR_MEMBER_NOT_FOUND_ID, PROPERTIES.getProperty(Constants.ERROR_MEMBER_NOT_FOUND)));

		serviceErrorMap.put(Constants.MULTIPLUS_SERVICE_CODE_409012, new ServiceError(Constants.ERROR_NOT_CATALOGED_ID,
				PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED)));

	}

}