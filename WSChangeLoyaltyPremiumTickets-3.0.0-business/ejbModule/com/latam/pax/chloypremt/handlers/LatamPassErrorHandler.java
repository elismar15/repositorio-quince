/**
 * 
 */
package com.latam.pax.chloypremt.handlers;

import java.util.HashMap;

import com.latam.pax.chloypremt.utils.Constants;
import com.siebel.customui.SiebelFault;

/**
 * Class to handle latam errors
 * 
 * @author Alberto Tejos S.
 *
 */
public final class LatamPassErrorHandler extends ErrorHandler {

	public LatamPassErrorHandler(SiebelFault siebelFault) {
		super(siebelFault);

		fillServiceErrorMap();

		ServiceError serviceError = getServiceError(
				Integer.parseInt(siebelFault.getFaultInfo().getErrorstack().getError().get(0).getErrorcode()));

		this.code = serviceError.getErrorCode();
		this.message = serviceError.getErrorMessage();
		this.nativeMessage = appendNativeMessage();
	}

	@Override
	protected void fillServiceErrorMap() {

		serviceErrorMap = new HashMap<>();

		serviceErrorMap.put(Integer.parseInt(Constants.LATAM_SERVICE_CODE_RD002),
				new ServiceError(Constants.ERROR_INSUFFICIENT_BALANCE_ID, Constants.ERROR_INSUFFICIENT_BALANCE));

		serviceErrorMap.put(Integer.parseInt(Constants.LATAM_SERVICE_CODE_RD100),
				new ServiceError(Constants.ERROR_MEMBER_NOT_FOUND_ID, Constants.ERROR_MEMBER_NOT_FOUND));

		serviceErrorMap.put(Integer.parseInt(Constants.LATAM_SERVICE_CODE_RD101), new ServiceError(
				Constants.ERROR_MEMBERSHIP_NUMBER_FORMAT_ID, Constants.ERROR_MEMBERSHIP_NUMBER_FORMAT));

		serviceErrorMap.put(Integer.parseInt(Constants.LATAM_SERVICE_CODE_RD105), new ServiceError(
				Constants.ERROR_AWARD_CODE_DOES_NOT_EXIST_ID, Constants.ERROR_AWARD_CODE_DOES_NOT_EXIST));

		serviceErrorMap.put(Integer.parseInt(Constants.LATAM_SERVICE_CODE_RD106),
				new ServiceError(Constants.ERROR_ACCOUNT_INACTIVE_ID, Constants.ERROR_ACCOUNT_INACTIVE));

	}

	@Override
	protected String appendNativeMessage() {

		StringBuilder builder = new StringBuilder();

		for (com.siebel.ws.fault.Error error : ((SiebelFault) exception).getFaultInfo().getErrorstack().getError()) {
			builder.append(printError(error.getErrormsg()));
		}

		return builder.toString();
	}

}
