
package com.latam.entities.v3_1;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * Entidad que representa datos del socio 
 * 
 * <p>Clase Java para LoyaltyMemberType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LoyaltyMemberType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ffNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="programCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoyaltyMemberType", propOrder = {
    "ffNumber",
    "programCode"
})
public class LoyaltyMemberType {

    @XmlElement(required = true)
    @NotEmpty(message="{rq.parameter.empty}")
    @NotNull(message="{rq.parameter.null}")
    protected String ffNumber;
    protected String programCode;

    /**
     * Obtiene el valor de la propiedad ffNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFfNumber() {
        return ffNumber;
    }

    /**
     * Define el valor de la propiedad ffNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFfNumber(String value) {
        this.ffNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad programCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * Define el valor de la propiedad programCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramCode(String value) {
        this.programCode = value;
    }

}
