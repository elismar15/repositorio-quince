package com.latam.pax.chloypremt.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class SourceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	@NotNull(message="{}")
	private String systemId;
	
	@Getter @Setter
	private String userId;
	
	@Getter @Setter
	private String transactionDateTime;
	
	@Getter @Setter
	private String ip;
	
}
