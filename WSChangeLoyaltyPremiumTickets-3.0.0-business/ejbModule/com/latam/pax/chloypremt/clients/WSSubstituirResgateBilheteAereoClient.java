package com.latam.pax.chloypremt.clients;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.domain.BookingDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.LoyaltyMemberDTO;
import com.latam.pax.chloypremt.domain.LoyaltyPremiumTicketsInfoDTO;
import com.latam.pax.chloypremt.domain.NewDTO;
import com.latam.pax.chloypremt.domain.OriginalDTO;
import com.latam.pax.chloypremt.domain.PassengerBookingTypeDTO;
import com.latam.pax.chloypremt.domain.SourceDTO;
import com.latam.pax.chloypremt.domain.TicketDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.handlers.LatamFidelidadeErrorHandler;
import com.latam.pax.chloypremt.handlers.WSSUsernameTokenSecurityHandler;
import com.latam.pax.chloypremt.utils.Constants;
import com.latam.pax.chloypremt.utils.DateUtil;
import com.latam.pax.chloypremt.utils.StringUtil;
import com.latam.ws.multiplus.subsresbil.BilheteEletronico;
import com.latam.ws.multiplus.subsresbil.BilheteSubstituicao;
import com.latam.ws.multiplus.subsresbil.BilheteSubstituicaoList;
import com.latam.ws.multiplus.subsresbil.Canal;
import com.latam.ws.multiplus.subsresbil.DadosCadastraisPF;
import com.latam.ws.multiplus.subsresbil.ErroTecnicoFaultMsg;
import com.latam.ws.multiplus.subsresbil.Maquina;
import com.latam.ws.multiplus.subsresbil.MetaInformacao;
import com.latam.ws.multiplus.subsresbil.Participante;
import com.latam.ws.multiplus.subsresbil.PedidoAereo;
import com.latam.ws.multiplus.subsresbil.Preco;
import com.latam.ws.multiplus.subsresbil.PropriedadesExecucao;
import com.latam.ws.multiplus.subsresbil.Sistema;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereoFaultMsg;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereoInput;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereoOutput;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereov1;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereov1PortType;
import com.latam.ws.multiplus.subsresbil.TipoReferencia;
import com.latam.ws.multiplus.subsresbil.Transacao;
import com.latam.ws.multiplus.subsresbil.Usuario;
import lombok.extern.slf4j.Slf4j;

/**
 * Substituir Resgate Bilhete Aereo Client implementation class
 * 
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public class WSSubstituirResgateBilheteAereoClient {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();
	private static final String DATE_FORMAT = "dd/MM/yyyy hh:MM:ss";
	private static final int ID_INTERFACE = 1;

	private static SubstituirResgateBilheteAereov1 service;

	public SubstituirResgateBilheteAereoOutput execute(ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {

		try {
			MetaInformacao metainfo = completeMetaInformacao();

			URL url = SubstituirResgateBilheteAereov1.class
					.getResource(PROPERTIES.getProperty(Constants.WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_WSDL));

			SubstituirResgateBilheteAereov1PortType port = getService(url).getSubstituirResgateBilheteAereov1();

			((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					PROPERTIES.getProperty(Constants.WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_ENDPOINT));

			SubstituirResgateBilheteAereoInput input = setInput(request);

			return port.substituirResgateBilheteAereo(input, metainfo);

		} catch (ErroTecnicoFaultMsg | SubstituirResgateBilheteAereoFaultMsg e) {
			logger.error(e.getMessage(), e);
			LatamFidelidadeErrorHandler errorHandler = new LatamFidelidadeErrorHandler(e);
			throw new ChangeLoyaltyPremiumTicketsException(errorHandler.getCode(), errorHandler.getMessage(),
					errorHandler.getNativeMessage());

		} catch (WebServiceException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_MULTIPLUS_SERVICE_ID,
					PROPERTIES.getProperty(Constants.ERROR_MULTIPLUS_SERVICE), e.getMessage());

		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_NOT_CATALOGED_ID,
					PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED), e.getMessage());

		}

	}

	public SubstituirResgateBilheteAereoInput setInput(ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {
		SubstituirResgateBilheteAereoInput substituirResgateBilheteAereoInput = new SubstituirResgateBilheteAereoInput();

		LoyaltyMemberDTO loyaltyMemberDTO = request.getLoyaltyMemberDTO();
		LoyaltyPremiumTicketsInfoDTO loyaltyPremiumTicketsInfoDTO = request.getLoyaltyPremiumTicketsInfoDTO();
		BookingDTO bookingDTO = loyaltyPremiumTicketsInfoDTO.getBookingDTO();
		List<TicketDTO> tickets = bookingDTO.getTickets();
		SourceDTO sourceDTO = request.getSourceDTO();

		PedidoAereo pedidoAereo = new PedidoAereo();

		Participante participante = new Participante();
		participante.setNumeroMultiplus(loyaltyMemberDTO.getFfNumber());

		pedidoAereo.setParticipante(participante);
		pedidoAereo.setIndicadorVoluntario(isVoluntary(loyaltyPremiumTicketsInfoDTO.getVoluntary()));

		BilheteSubstituicaoList bilheteSubstituicaoList = new BilheteSubstituicaoList();

		setInputTickets(tickets, bilheteSubstituicaoList);

		pedidoAereo.setListaBilheteSubstituicao(bilheteSubstituicaoList);
		pedidoAereo.setCodigoReserva(bookingDTO.getRecordLocator());
		pedidoAereo.setPontoDeVenda(bookingDTO.getPointOfSale());
		pedidoAereo.setCodigoAutorizacao(loyaltyPremiumTicketsInfoDTO.getAuthorizationId());

		TipoReferencia tipoReferencia = new TipoReferencia();
		Canal canal = new Canal();

		tipoReferencia.setValorReferencia(loyaltyPremiumTicketsInfoDTO.getChannelDTO().getChannelName());
		canal.setCanal(tipoReferencia);

		TipoReferencia tipoReferencia2 = new TipoReferencia();
		tipoReferencia2.setValorReferencia(loyaltyPremiumTicketsInfoDTO.getChannelDTO().getSubChannelName());
		canal.setSubcanal(tipoReferencia2);

		pedidoAereo.setCanal(canal);

		Sistema sistema = new Sistema();
		sistema.setIdSistema(sourceDTO.getSystemId());

		Usuario usuario = new Usuario();
		usuario.setIdUsuario(sourceDTO.getUserId());

		sistema.setUsuario(usuario);

		if (null != sourceDTO && null != sourceDTO.getTransactionDateTime()
				&& !sourceDTO.getTransactionDateTime().isEmpty()) {
			XMLGregorianCalendar calendar = DateUtil
					.stringDateToXMLGrogorianCalendar(sourceDTO.getTransactionDateTime(), DATE_FORMAT);
			sistema.setDataHora(calendar);
		}

		Maquina maquina = new Maquina();
		maquina.setEnderecoIp(sourceDTO.getIp());

		sistema.setMaquina(maquina);

		pedidoAereo.setSistemaOrigem(sistema);

		PropriedadesExecucao propriedadesExecucao = new PropriedadesExecucao();
		propriedadesExecucao.setIdInterface(ID_INTERFACE);

		substituirResgateBilheteAereoInput.setPedidoAereo(pedidoAereo);
		substituirResgateBilheteAereoInput.setPropriedadesExecucao(propriedadesExecucao);

		return substituirResgateBilheteAereoInput;
	}

	/**
	 * 
	 * @param tickets
	 * @param bilheteSubstituicaoList
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	private void setInputTickets(List<TicketDTO> tickets, BilheteSubstituicaoList bilheteSubstituicaoList)
			throws ChangeLoyaltyPremiumTicketsException {

		if (null != tickets && !tickets.isEmpty()) {
			for (TicketDTO ticketDTO : tickets) {

				OriginalDTO originalDTO = ticketDTO.getOriginalDTO();
				PassengerBookingTypeDTO passengerBookingTypeDTO = originalDTO.getPassengerBookingTypeDTO();

				BilheteEletronico originalBilheteEletronico = new BilheteEletronico();
				originalBilheteEletronico.setNumeroBilhete(ticketDTO.getTicketNumber());

				DadosCadastraisPF originalDadosCadastraisPF = new DadosCadastraisPF();
				originalDadosCadastraisPF.setPrimeiroNome(passengerBookingTypeDTO.getFirstName());
				originalDadosCadastraisPF.setSobrenome(passengerBookingTypeDTO.getLastName());

				originalBilheteEletronico.setDadosPassageiro(originalDadosCadastraisPF);

				TipoReferencia originalTipoPassageiro = new TipoReferencia();
				originalTipoPassageiro.setCodigoValorReferencia(passengerBookingTypeDTO.getPassengerTypeCode());

				originalBilheteEletronico.setTipoPassageiro(originalTipoPassageiro);

				Transacao transacao = new Transacao();

				String loyaltyCertificateNumber;
				if (originalDTO.getLoyaltyCertificateNumber().contains("-")) {
					loyaltyCertificateNumber = StringUtil.hyphenSplit(originalDTO.getLoyaltyCertificateNumber(), 1);
				} else {
					loyaltyCertificateNumber = originalDTO.getLoyaltyCertificateNumber();
				}

				transacao.setNumeroTransacao(loyaltyCertificateNumber);

				originalBilheteEletronico.setTransacaoResgate(transacao);

				Preco originalPrecoPontosVoados = new Preco();
				originalPrecoPontosVoados.setPontos(Long.parseLong(originalDTO.getPointsDTO().getUsed()));

				originalBilheteEletronico.setPrecoPontosVoados(originalPrecoPontosVoados);

				Preco originalPrecoUnitario = new Preco();
				originalPrecoUnitario.setPontos(Long.parseLong(originalDTO.getPointsDTO().getExchanged()));

				originalBilheteEletronico.setPrecoUnitario(originalPrecoUnitario);

				BilheteSubstituicao bilheteSubstituicao = new BilheteSubstituicao();
				bilheteSubstituicao.setBilheteOriginal(originalBilheteEletronico);

				NewDTO new1 = ticketDTO.getNewDTO();

				BilheteEletronico newBilheteEletronico = new BilheteEletronico();
				newBilheteEletronico.setNumeroPassageiro(Integer.parseInt(new1.getPassenger().getPaxNameNumber()));

				DadosCadastraisPF newDadosCadastraisPF = new DadosCadastraisPF();
				newDadosCadastraisPF.setPrimeiroNome(new1.getPassenger().getFirstName());
				newDadosCadastraisPF.setSobrenome(new1.getPassenger().getLastName());

				newBilheteEletronico.setDadosPassageiro(newDadosCadastraisPF);

				TipoReferencia newTipoPassageiro = new TipoReferencia();
				newTipoPassageiro.setCodigoValorReferencia(new1.getPassenger().getPassengerTypeCode());

				newBilheteEletronico.setTipoPassageiro(newTipoPassageiro);

				Preco newPreco = new Preco();
				newPreco.setPontos(Long.parseLong(new1.getPricePoints()));

				newBilheteEletronico.setPrecoUnitario(newPreco);

				bilheteSubstituicao.setNovoBilhete(newBilheteEletronico);

				bilheteSubstituicaoList.getBilheteSubstituicao().add(bilheteSubstituicao);
			}
		}
	}

	public SubstituirResgateBilheteAereov1 getService(URL url) {
		synchronized (WSSubstituirResgateBilheteAereoClient.class) {
			if (service == null) {
				service = new SubstituirResgateBilheteAereov1(url);
				service.setHandlerResolver(new HandlerResolver() {
					@SuppressWarnings("rawtypes")
					@Override
					public List<Handler> getHandlerChain(PortInfo portInfo) {
						List<Handler> handlerList = new ArrayList<>();
						handlerList.add(new WSSUsernameTokenSecurityHandler(
								PROPERTIES.getProperty(Constants.WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_USER),
								PROPERTIES.getProperty(Constants.WS_SUBSTITUIR_RESGATE_BILHETE_AEREO_PASS)));
						return handlerList;
					}
				});
			}
			return service;
		}
	}

	/**
	 * Complete MetaInformacao from service properties
	 * 
	 * @return MetaInformacao mapped
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	private MetaInformacao completeMetaInformacao() throws ChangeLoyaltyPremiumTicketsException {

		MetaInformacao metaInformacao = new MetaInformacao();
		metaInformacao.setIdRequisicao(PROPERTIES.getProperty(Constants.WS_META_INFO_ID_REQUIREMENT));
		metaInformacao.setDataHoraRequisicao(DateUtil.obtainXMLGregorianCalendarFromSysdate());
		metaInformacao.setSistemaOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_SYSTEM));
		metaInformacao.setOperacaoOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_OPERATION));
		metaInformacao.setEnderecoOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_ADDRESS));
		metaInformacao.setUsuarioOrigem(PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_USER));
		metaInformacao.setCanalOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_VERSION));
		metaInformacao.setVersaoConsumidor(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_TYPE));
		metaInformacao
				.setVersaoServicoConsumido(PROPERTIES.getProperty(Constants.WS_META_INFO_SERVICE_CONSUMED_VERSION));
		metaInformacao.setTipoConsumidor(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_TYPE));
		metaInformacao.setEnderecoServicoConsumido(
				PROPERTIES.getProperty(Constants.WS_META_INFO_SERVICE_CONSUMED_ADDRESS_SUBST));

		return metaInformacao;
	}

	/**
	 * Check if indicador voluntario is VOLUNTARIA or INVOLUNTARIA
	 * 
	 * @param voluntary
	 * @return
	 */
	private boolean isVoluntary(String voluntary) {
		if (!StringUtil.isNullOrEmpty(voluntary)) {
			return Constants.VOLUNTARY.equalsIgnoreCase(voluntary) ? true : false;
		}

		return false;
	}

}
