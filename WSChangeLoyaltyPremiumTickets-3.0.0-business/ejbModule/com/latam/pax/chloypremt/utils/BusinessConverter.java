/**
 * 
 */
package com.latam.pax.chloypremt.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRSDTO;
import com.latam.pax.chloypremt.domain.ExchangedDTO;
import com.latam.pax.chloypremt.domain.LoyaltyPremiumTicketDTO;
import com.latam.pax.chloypremt.domain.NewDTO;
import com.latam.pax.chloypremt.domain.OriginalDTO;
import com.latam.pax.chloypremt.domain.PointsDTO;
import com.latam.pax.chloypremt.domain.ReAccreditedDTO;
import com.latam.pax.chloypremt.domain.ServiceStatusTypeDTO;
import com.latam.pax.chloypremt.domain.TicketDTO;
import com.latam.pax.chloypremt.domain.TransactionDTO;
import com.latam.ws.multiplus.subsresbil.BilheteEletronico;
import com.latam.ws.multiplus.subsresbil.BilheteSubstituicao;
import com.latam.ws.multiplus.subsresbil.BilheteSubstituicaoList;
import com.latam.ws.multiplus.subsresbil.PedidoAereo;
import com.latam.ws.multiplus.subsresbil.SubstituirResgateBilheteAereoOutput;
import com.siebel.customui.ChangeExecutionOutput;
import com.siebel.xml.ocschangeexeoutput.data.ListOfOcschangeexeoutputData;
import com.siebel.xml.ocschangeexeoutput.data.LoyMemberData;

/**
 * Convert businnes logic domain response to service domain response
 * 
 * @author Alberto Tejos S.
 *
 */
public final class BusinessConverter {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	private BusinessConverter() {
	}

	/**
	 * Convert Change Execution client response domain to service response
	 * domain
	 * 
	 * @param changeExecutionOutput
	 * @return ChangeLoyaltyPremiumTicketsRSDTO
	 */
	public static ChangeLoyaltyPremiumTicketsRSDTO changeOutputToResponseDomain(
			ChangeExecutionOutput changeExecutionOutput) {

		ChangeLoyaltyPremiumTicketsRSDTO changeLoyaltyPremiumTicketsRSDTO = new ChangeLoyaltyPremiumTicketsRSDTO();

		ListOfOcschangeexeoutputData listOfOcschangeexeoutputData = changeExecutionOutput.getListOfOcschangeexeoutput();

		LoyMemberData loyMemberData = listOfOcschangeexeoutputData.getLoyMember();

		TicketDTO ticketDTO = new TicketDTO();
		ticketDTO.setTicketNumber(loyMemberData.getTicketNumber());

		OriginalDTO originalDTO = new OriginalDTO();
		ticketDTO.setOriginalDTO(originalDTO);

		PointsDTO pointsDTO = new PointsDTO();
		originalDTO.setPointsDTO(pointsDTO);

		ExchangedDTO exchangedDTO = new ExchangedDTO();
		exchangedDTO.setAmount(loyMemberData.getTotalPointsTxn().toString());
		exchangedDTO.setTransactionId(loyMemberData.getTxnNumber());
		pointsDTO.setExchangedDTO(exchangedDTO);

		TransactionDTO transactionDTO = new TransactionDTO();
		transactionDTO.setCancelId(loyMemberData.getDevolutionTxnNumber());
		originalDTO.setTransactionDTO(transactionDTO);

		NewDTO newDTO = new NewDTO();
		newDTO.setLoyaltyCertificateNumber(loyMemberData.getCPNumber());
		newDTO.setLoyaltyCertificateCode(loyMemberData.getCPNumber());

		ticketDTO.setNewDTO(newDTO);

		List<TicketDTO> tickets = new ArrayList<>();
		tickets.add(ticketDTO);

		LoyaltyPremiumTicketDTO loyaltyPremiumTicketDTO = new LoyaltyPremiumTicketDTO();
		loyaltyPremiumTicketDTO.setTickets(tickets);

		ServiceStatusTypeDTO serviceStatusTypeDTO = new ServiceStatusTypeDTO();
		serviceStatusTypeDTO.setCode(Constants.SUCCESSFUL_OPERATION_ID);
		serviceStatusTypeDTO.setMessage(PROPERTIES.getProperty(Constants.SUCCESSFUL_OPERATION));

		changeLoyaltyPremiumTicketsRSDTO.setLoyaltyPremiumTicketDTO(loyaltyPremiumTicketDTO);
		changeLoyaltyPremiumTicketsRSDTO.setServiceStatusTypeDTO(serviceStatusTypeDTO);

		return changeLoyaltyPremiumTicketsRSDTO;

	}

	/**
	 * Convert Substituir Resgate Bilhete Aereo client response domain to
	 * service response domain
	 * 
	 * @param substituirOutput
	 * @return ChangeLoyaltyPremiumTicketsRSDTO
	 */
	public static ChangeLoyaltyPremiumTicketsRSDTO substituirOutputToResponseDomain(
			SubstituirResgateBilheteAereoOutput substituirOutput) {

		ChangeLoyaltyPremiumTicketsRSDTO changeLoyaltyPremiumTicketsRSDTO = new ChangeLoyaltyPremiumTicketsRSDTO();

		List<BilheteSubstituicao> bilheteSubstituicaos = setListBilheteSubtituicao(substituirOutput);

		List<TicketDTO> tickets = new ArrayList<>();

		if (null != bilheteSubstituicaos && !bilheteSubstituicaos.isEmpty()) {

			String reciboEntrega = substituirOutput.getReciboEntrega();

			for (BilheteSubstituicao bilheteSubstituicao : bilheteSubstituicaos) {

				BilheteEletronico bilheteOriginal = bilheteSubstituicao.getBilheteOriginal();
				BilheteEletronico novoBilhete = bilheteSubstituicao.getNovoBilhete();

				TicketDTO ticketDTO = new TicketDTO();

				if (null != bilheteOriginal) {
					setDomainOriginalTickets(tickets, bilheteSubstituicao, bilheteOriginal, ticketDTO);
				} else if (null != novoBilhete) {
					setDomainNewTickets(tickets, novoBilhete, ticketDTO);
				}

			}

			LoyaltyPremiumTicketDTO loyaltyPremiumTicketDTO = new LoyaltyPremiumTicketDTO();
			loyaltyPremiumTicketDTO.setTickets(tickets);
			loyaltyPremiumTicketDTO.setReceptId(reciboEntrega);

			ServiceStatusTypeDTO serviceStatusTypeDTO = new ServiceStatusTypeDTO();
			serviceStatusTypeDTO.setCode(Constants.SUCCESSFUL_OPERATION_ID);
			serviceStatusTypeDTO.setMessage(PROPERTIES.getProperty(Constants.SUCCESSFUL_OPERATION));

			changeLoyaltyPremiumTicketsRSDTO.setServiceStatusTypeDTO(serviceStatusTypeDTO);
			changeLoyaltyPremiumTicketsRSDTO.setLoyaltyPremiumTicketDTO(loyaltyPremiumTicketDTO);

		} else {
			ServiceStatusTypeDTO serviceStatusTypeDTO = new ServiceStatusTypeDTO();
			serviceStatusTypeDTO.setCode(Constants.ERROR_NOT_CATALOGED_ID);
			serviceStatusTypeDTO.setMessage(PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED));
			changeLoyaltyPremiumTicketsRSDTO.setServiceStatusTypeDTO(serviceStatusTypeDTO);
		}

		return changeLoyaltyPremiumTicketsRSDTO;
	}

	/**
	 * Set List of Bilhete Subtituicao
	 * 
	 * @param substituirOutput
	 * @return List<BilheteSubstituicao>
	 */
	private static List<BilheteSubstituicao> setListBilheteSubtituicao(
			SubstituirResgateBilheteAereoOutput substituirOutput) {

		List<BilheteSubstituicao> bilheteSubstituicaos = new ArrayList<>();

		if (null != substituirOutput.getPedidoAereo()) {
			PedidoAereo pedidoAereo = substituirOutput.getPedidoAereo();

			if (null != pedidoAereo && null != pedidoAereo.getListaBilheteSubstituicao()) {
				BilheteSubstituicaoList bilheteSubstituicaoList = pedidoAereo.getListaBilheteSubstituicao();

				if (null != bilheteSubstituicaoList) {
					return bilheteSubstituicaoList.getBilheteSubstituicao();
				}

			}
		}

		return bilheteSubstituicaos;
	}

	/**
	 * Set new tickets to service response domain
	 * 
	 * @param tickets
	 * @param novoBilhete
	 * @param ticketDTO
	 */
	private static void setDomainNewTickets(List<TicketDTO> tickets, BilheteEletronico novoBilhete,
			TicketDTO ticketDTO) {
		String numeroPassageiro = null;

		if (null != novoBilhete.getNumeroPassageiro()) {
			numeroPassageiro = novoBilhete.getNumeroPassageiro().toString();
		}

		String numeroTransacao = null;

		if (null != novoBilhete.getTransacaoResgate()
				&& null != novoBilhete.getTransacaoResgate().getNumeroTransacao()) {
			numeroTransacao = novoBilhete.getTransacaoResgate().getNumeroTransacao();
		}

		NewDTO newDTO = new NewDTO();
		newDTO.setPaxNameNumber(numeroPassageiro);
		newDTO.setLoyaltyCertificateNumber(numeroTransacao);
		ticketDTO.setNewDTO(newDTO);

		tickets.add(ticketDTO);
	}

	/**
	 * Set domain original tickets to service domain original tickets of
	 * response
	 * 
	 * @param tickets
	 * @param bilheteSubstituicao
	 * @param bilheteOriginal
	 * @param ticketDTO
	 */
	private static void setDomainOriginalTickets(List<TicketDTO> tickets, BilheteSubstituicao bilheteSubstituicao,
			BilheteEletronico bilheteOriginal, TicketDTO ticketDTO) {

		OriginalDTO originalDTO = setOriginal(bilheteOriginal);

		String numeroBilhete = setNumeroBilhete(bilheteOriginal);

		Long pontos = setPontos(bilheteOriginal);

		String numeroTransacaoResgate = setNumeroTransacaoResgate(bilheteOriginal);

		String numeroTransacaoCancelamiento = setNumeroTransacaoCancelamiento(bilheteOriginal);

		PointsDTO pointsDTO = setPoints(pontos, originalDTO);

		setExchanged(pontos, numeroTransacaoResgate, pointsDTO);

		String numeroTransacaoRecredito = setNumeroTransacaoRecredito(bilheteSubstituicao);

		setReaccredited(bilheteSubstituicao, numeroTransacaoRecredito, pointsDTO);

		String numeroTransacaoPontosVolados = setNumeroTransacaoPontosVolados(bilheteOriginal);

		TransactionDTO transactionDTO = setTransaction(originalDTO, numeroTransacaoCancelamiento,
				numeroTransacaoPontosVolados);

		originalDTO.setTransactionDTO(transactionDTO);

		ticketDTO.setTicketNumber(numeroBilhete);
		ticketDTO.setOriginalDTO(originalDTO);
		tickets.add(ticketDTO);
	}

	/**
	 * Set domain points of response domain
	 * 
	 * @param pontos
	 * @param originalDTO
	 * @return pointsDTO
	 */
	private static PointsDTO setPoints(Long pontos, OriginalDTO originalDTO) {
		PointsDTO pointsDTO = null;

		if (null != originalDTO && null != pontos) {
			pointsDTO = new PointsDTO();
			originalDTO.setPointsDTO(pointsDTO);
		}

		return pointsDTO;
	}

	/**
	 * Set domain transaction of response
	 * 
	 * @param originalDTO
	 * @param numeroTransacaoCancelamiento
	 * @param numeroTransacaoPontosVolados
	 * @return transactionDTO
	 */
	private static TransactionDTO setTransaction(OriginalDTO originalDTO, String numeroTransacaoCancelamiento,
			String numeroTransacaoPontosVolados) {
		TransactionDTO transactionDTO = null;

		if (null != originalDTO) {
			transactionDTO = new TransactionDTO();
		}

		if (null != transactionDTO && null != numeroTransacaoCancelamiento) {
			transactionDTO.setCancelId(numeroTransacaoCancelamiento);
		}

		if (null != transactionDTO && null != numeroTransacaoPontosVolados) {
			transactionDTO.setUsedId(numeroTransacaoPontosVolados);
		}

		return transactionDTO;
	}

	/**
	 * Set domain original ticket of response
	 * 
	 * @param bilheteOriginal
	 * @return originalDTO
	 */
	private static OriginalDTO setOriginal(BilheteEletronico bilheteOriginal) {
		OriginalDTO originalDTO = null;

		if (null != bilheteOriginal) {
			originalDTO = new OriginalDTO();
		}
		return originalDTO;
	}

	/**
	 * Set numero transacao pontos volados of response
	 * 
	 * @param bilheteOriginal
	 * @return numeroTransacaoPontosVolados
	 */
	private static String setNumeroTransacaoPontosVolados(BilheteEletronico bilheteOriginal) {
		String numeroTransacaoPontosVolados = null;

		if (null != bilheteOriginal && null != bilheteOriginal.getTransacaoPontosVoados()) {
			numeroTransacaoPontosVolados = bilheteOriginal.getTransacaoPontosVoados().getNumeroTransacao();
		}
		return numeroTransacaoPontosVolados;
	}

	/**
	 * Set domain reaccredited of response
	 * 
	 * @param bilheteSubstituicao
	 * @param numeroTransacaoRecredito
	 */
	private static void setReaccredited(BilheteSubstituicao bilheteSubstituicao, String numeroTransacaoRecredito,
			PointsDTO pointsDTO) {
		Long pontosRecredito = null;

		if (null != bilheteSubstituicao.getTransacaoRecredito()
				&& null != bilheteSubstituicao.getTransacaoRecredito().getTotalTransacao()
				&& null != bilheteSubstituicao.getTransacaoRecredito().getTotalTransacao().getPontos()) {
			pontosRecredito = bilheteSubstituicao.getTransacaoRecredito().getTotalTransacao().getPontos();
		}

		ReAccreditedDTO reAccreditedDTO = new ReAccreditedDTO();

		if (null != pontosRecredito) {
			reAccreditedDTO.setAmount(pontosRecredito.toString());
		}

		if (null != numeroTransacaoRecredito) {
			reAccreditedDTO.setTransactionId(numeroTransacaoRecredito);
		}

		if (null != pointsDTO) {
			pointsDTO.setReAccreditedDTO(reAccreditedDTO);
		}

	}

	/**
	 * Set numero transacao recredito of response
	 * 
	 * @param bilheteSubstituicao
	 * @return numeroTransacaoRecredito
	 */
	private static String setNumeroTransacaoRecredito(BilheteSubstituicao bilheteSubstituicao) {
		String numeroTransacaoRecredito = null;

		if (null != bilheteSubstituicao.getTransacaoRecredito()
				&& null != bilheteSubstituicao.getTransacaoRecredito().getNumeroTransacao()) {
			numeroTransacaoRecredito = bilheteSubstituicao.getTransacaoRecredito().getNumeroTransacao();
		}
		return numeroTransacaoRecredito;
	}

	/**
	 * Set domain exchanged of response
	 * 
	 * @param pontos
	 * @param numeroTransacaoResgate
	 * @param pointsDTO
	 */
	private static void setExchanged(Long pontos, String numeroTransacaoResgate, PointsDTO pointsDTO) {
		ExchangedDTO exchangedDTO = new ExchangedDTO();

		if (null != pontos) {
			exchangedDTO.setAmount(pontos.toString());
		}

		exchangedDTO.setTransactionId(numeroTransacaoResgate);

		if (null != pointsDTO) {
			pointsDTO.setExchangedDTO(exchangedDTO);
		}

	}

	/**
	 * Set numero transacao cancelamiento of response
	 * 
	 * @param bilheteOriginal
	 * @return numeroTransacaoCancelamiento
	 */
	private static String setNumeroTransacaoCancelamiento(BilheteEletronico bilheteOriginal) {
		String numeroTransacaoCancelamiento = null;

		if (null != bilheteOriginal.getTransacaoResgate().getTransacaoCancelamento()
				&& null != bilheteOriginal.getTransacaoResgate().getTransacaoCancelamento().getNumeroTransacao()) {
			numeroTransacaoCancelamiento = bilheteOriginal.getTransacaoResgate().getTransacaoCancelamento()
					.getNumeroTransacao();
		}
		return numeroTransacaoCancelamiento;
	}

	/**
	 * Set numero transacao resgate of response
	 * 
	 * @param bilheteOriginal
	 * @return numeroTransacaoResgate
	 */
	private static String setNumeroTransacaoResgate(BilheteEletronico bilheteOriginal) {
		String numeroTransacaoResgate = null;

		if (null != bilheteOriginal.getTransacaoResgate()
				&& null != bilheteOriginal.getTransacaoResgate().getNumeroTransacao()) {
			numeroTransacaoResgate = bilheteOriginal.getTransacaoResgate().getNumeroTransacao();
		}
		return numeroTransacaoResgate;
	}

	/**
	 * Set pontos of response
	 * 
	 * @param bilheteOriginal
	 * @return pontos
	 */
	private static Long setPontos(BilheteEletronico bilheteOriginal) {
		Long pontos = null;

		if (null != bilheteOriginal && null != bilheteOriginal.getPrecoUnitario()
				&& null != bilheteOriginal.getPrecoUnitario().getPontos()) {
			pontos = bilheteOriginal.getPrecoUnitario().getPontos();
		}
		return pontos;
	}

	/**
	 * Set numero bilhete of response
	 * 
	 * @param bilheteOriginal
	 * @return numeroBilhete
	 */
	private static String setNumeroBilhete(BilheteEletronico bilheteOriginal) {
		String numeroBilhete = null;

		if (null != bilheteOriginal.getNumeroBilhete()) {
			numeroBilhete = bilheteOriginal.getNumeroBilhete();
		}
		return numeroBilhete;
	}
}
