package com.latam.pax.chloypremt.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class SessionContextDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	@NotNull(message="{}")
	private String authorizationType;
	
	@Getter @Setter
	private String originTransactionId;
	
	@Getter @Setter
	private SystemDTO systemDTO;
	
	@Getter @Setter
	private List<ContextDTO> contexts;
	
	@Getter @Setter
	private String idSessionAuthorization;
}
