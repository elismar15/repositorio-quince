/**
 * 
 */
package com.latam.pax.chloypremt.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class LoyaltyPremiumTicketsInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Getter @Setter
	@NotNull(message="{}")
	private BookingDTO bookingDTO;
	
	@Getter @Setter
	private ChannelDTO channelDTO;
	
	@Getter @Setter
	private SessionContextDTO sessionContextDTO;
	
	@Getter @Setter
	private String penaltyPoints;
	
	@Getter @Setter
	private String voluntary;
	
	@Getter @Setter
	private String authorizationId;
	
}
