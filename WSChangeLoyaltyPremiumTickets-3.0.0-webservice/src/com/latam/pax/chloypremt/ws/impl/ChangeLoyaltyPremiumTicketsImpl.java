package com.latam.pax.chloypremt.ws.impl;

import java.util.Calendar;
import java.util.Properties;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.arq.commons.ws.abstracts.LATAMWebService;
import com.latam.arq.commons.ws.domain.LATAMWebServicesConstants;
import com.latam.arq.commons.ws.exceptions.LATAMSoapException;
import com.latam.entities.v3_0.ServiceStatusType;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRSDTO;
import com.latam.pax.chloypremt.ejb.services.ChangeLoyaltyPremiumTicketsServices;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.utils.Constants;
import com.latam.pax.chloypremt.utils.Converter;
import com.latam.pax.chloypremt.utils.LogUtil;
import com.latam.pax.chloypremt.utils.Validator;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRQ;
import com.latam.ws.v3_0.ChangeLoyaltyPremiumTicketsRS;
import lombok.extern.slf4j.Slf4j;

/**
 * WS implementation of ChangeLoyaltyPremiumTickets-3.0.0
 * 
 * @author ANT using import-ws target
 * @author LATAM Enterprise Architect
 * @author Alberto Tejos S.
 */
@WebService(portName = "ChangeLoyaltyPremiumTicketsPortType", name = "ChangeLoyaltyPremiumTickets-3.0.0", targetNamespace = LATAMWebServicesConstants.NS_SVC_LATAM
		+ "v3_0", serviceName = "ChangeLoyaltyPremiumTickets-3.0.0", wsdlLocation = "WEB-INF/wsdl/ChangeLoyaltyPremiumTickets-3.0.0.wsdl")
@Slf4j
@SOAPBinding(parameterStyle = ParameterStyle.BARE)
public class ChangeLoyaltyPremiumTicketsImpl extends LATAMWebService {

	@EJB
	private transient ChangeLoyaltyPremiumTicketsServices ejbBusinessService;

	/**
	 * Implementation of the "changeLoyaltyPremiumTickets" webservice method
	 * 
	 * @param request
	 *            The request using an object of ChangeLoyaltyPremiumTicketsRQ
	 *            type.
	 * @return Any object of type ChangeLoyaltyPremiumTicketsRS that represent
	 *         the service response.
	 */
	@WebMethod
	@WebResult(name = "ChangeLoyaltyPremiumTicketsRS", targetNamespace = LATAMWebServicesConstants.NS_SVC_LATAM
			+ "v3_0", partName = "ChangeLoyaltyPremiumTicketsRS")
	public ChangeLoyaltyPremiumTicketsRS changeLoyaltyPremiumTickets(
			@WebParam(name = "ChangeLoyaltyPremiumTicketsRQ", targetNamespace = LATAMWebServicesConstants.NS_SVC_LATAM
					+ "v3_0", partName = "ChangeLoyaltyPremiumTicketsRQ") final ChangeLoyaltyPremiumTicketsRQ request) {

		ChangeLoyaltyPremiumTicketsRS changeLoyaltyPremiumTicketsRS = new ChangeLoyaltyPremiumTicketsRS();
		ServiceStatusType serviceStatusType = new ServiceStatusType();

		String rqXml = LogUtil.objectToXML(request);
		Properties properties = AppConfigUtil.getApplicationProperties();

		final long t1 = System.currentTimeMillis();
		final Calendar startCal = Calendar.getInstance();
		final String startDate = LogUtil.LOG_DATE_FORMAT.format(startCal.getTime());

		String channelName = null;
		String serviceType = null;

		try {
			validateRequestHeaders();
			Validator.validateRequest(request);

			if (logger.isDebugEnabled()) {
				logger.debug("LAN-ApplicationName: " + getAppName());
			}

			ChangeLoyaltyPremiumTicketsRQDTO changeLoyaltyPremiumTicketsRQDTO = Converter.serviceToRequestDomain(request);
			channelName = getChannelName(changeLoyaltyPremiumTicketsRQDTO);
			serviceType = getServiceType(changeLoyaltyPremiumTicketsRQDTO);

			ChangeLoyaltyPremiumTicketsRSDTO changeLoyaltyPremiumTicketsRSDTO = ejbBusinessService
					.execute(changeLoyaltyPremiumTicketsRQDTO);

			final long t2 = System.currentTimeMillis();

			if (null != changeLoyaltyPremiumTicketsRSDTO.getServiceStatusTypeDTO() && changeLoyaltyPremiumTicketsRSDTO
					.getServiceStatusTypeDTO().getCode() == Constants.SUCCESSFUL_OPERATION_ID) {
				changeLoyaltyPremiumTicketsRS = Converter.domainResponseToServiceResponse(changeLoyaltyPremiumTicketsRSDTO);

				logger.info(LogUtil.createLog(startDate, t2 - t1, properties.getProperty(Constants.RESPONSE_STATUS_OK),
						getAppName(), channelName, Constants.SUCCESSFUL_OPERATION_ID, null, null, serviceType));

			}

			serviceStatusType = Converter.convertServiceStatus(changeLoyaltyPremiumTicketsRSDTO);
			changeLoyaltyPremiumTicketsRS.setServiceStatus(serviceStatusType);

		} catch (LATAMSoapException e) {
			logger.error(e.getMessage(), e);
			changeLoyaltyPremiumTicketsRS.setServiceStatus(e.getServiceStatus());

			final long t2 = System.currentTimeMillis();

			logger.info(LogUtil.createLog(startDate, t2 - t1, properties.getProperty(Constants.RESPONSE_STATUS_NOK),
					getAppName(), channelName, e.getServiceStatus().getCode(), rqXml, e.getMessage(), serviceType));

		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);

			final long t2 = System.currentTimeMillis();

			logger.info(LogUtil.createLog(startDate, t2 - t1, properties.getProperty(Constants.RESPONSE_STATUS_NOK),
					getAppName(), channelName, e.getCode(), rqXml, e.getMessage(), serviceType));

			serviceStatusType.setCode(e.getCode());
			serviceStatusType.setMessage(e.getMessage());
			serviceStatusType.setNativeMessage(e.getNativeMessage());
			changeLoyaltyPremiumTicketsRS.setServiceStatus(serviceStatusType);
		}

		long t2 = System.currentTimeMillis();

		if (logger.isDebugEnabled()) {
			logger.debug("Total execution time: " + (t2 - t1));
		}

		return changeLoyaltyPremiumTicketsRS;

	}

	/**
	 * Get service type
	 * @param changeLoyaltyPremiumTicketsRQDTO
	 * @return String service type
	 */
	private String getServiceType(final ChangeLoyaltyPremiumTicketsRQDTO changeLoyaltyPremiumTicketsRQDTO) {
		final String programCode = changeLoyaltyPremiumTicketsRQDTO.getLoyaltyMemberDTO().getProgramCode();

		if (null != programCode && !programCode.isEmpty()) {
			return programCode;
		} else {
			return "*";
		}
	}

	/**
	 * Get channel name
	 * @param channel
	 * @return String channel name
	 */
	private String getChannelName(final ChangeLoyaltyPremiumTicketsRQDTO changeLoyaltyPremiumTicketsRQDTO) {
		final String channelName = changeLoyaltyPremiumTicketsRQDTO.getLoyaltyPremiumTicketsInfoDTO().getChannelDTO()
				.getChannelName();
		final String programCode = getServiceType(changeLoyaltyPremiumTicketsRQDTO);

		if (programCode.equalsIgnoreCase(Constants.LATAM_FIDELIDADE) && null != channelName
				&& !channelName.isEmpty()) {
			return channelName;
		} else if (programCode.equalsIgnoreCase(Constants.LATAMPASS) && null != getAppName()) {
			return getAppName();
		} else {
			return "*";
		}

	}

}
