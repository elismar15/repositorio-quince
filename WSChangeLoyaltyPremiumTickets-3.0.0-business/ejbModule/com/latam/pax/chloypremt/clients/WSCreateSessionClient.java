package com.latam.pax.chloypremt.clients;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.ContextDTO;
import com.latam.pax.chloypremt.domain.LoyaltyMemberDTO;
import com.latam.pax.chloypremt.domain.LoyaltyPremiumTicketsInfoDTO;
import com.latam.pax.chloypremt.domain.SessionContextDTO;
import com.latam.pax.chloypremt.domain.SystemDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import com.latam.pax.chloypremt.handlers.LatamFidelidadeErrorHandler;
import com.latam.pax.chloypremt.handlers.WSSUsernameTokenSecurityHandler;
import com.latam.pax.chloypremt.utils.Constants;
import com.latam.pax.chloypremt.utils.DateUtil;
import com.latam.ws.multiplus.criarsessa.Atributo;
import com.latam.ws.multiplus.criarsessa.AtributoList;
import com.latam.ws.multiplus.criarsessa.CriarSessaoAutorizacaoParticipanteFaultMsg;
import com.latam.ws.multiplus.criarsessa.CriarSessaoAutorizacaoParticipanteInput;
import com.latam.ws.multiplus.criarsessa.CriarSessaoAutorizacaoParticipanteOutput;
import com.latam.ws.multiplus.criarsessa.CriarSessaoAutorizacaoParticipantev1;
import com.latam.ws.multiplus.criarsessa.CriarSessaoAutorizacaoParticipantev1PortType;
import com.latam.ws.multiplus.criarsessa.ErroTecnicoFaultMsg;
import com.latam.ws.multiplus.criarsessa.MetaInformacao;
import com.latam.ws.multiplus.criarsessa.Participante;
import com.latam.ws.multiplus.criarsessa.SessaoAutorizacao;
import com.latam.ws.multiplus.criarsessa.Sistema;
import com.latam.ws.multiplus.criarsessa.TipoReferencia;
import com.latam.ws.multiplus.criarsessa.Transacao;
import lombok.extern.slf4j.Slf4j;

/**
 * Create Session Client implementation class
 * 
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public class WSCreateSessionClient {

	private static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();

	private static CriarSessaoAutorizacaoParticipantev1 service;

	public CriarSessaoAutorizacaoParticipanteOutput execute(ChangeLoyaltyPremiumTicketsRQDTO request)
			throws ChangeLoyaltyPremiumTicketsException {
		CriarSessaoAutorizacaoParticipanteOutput response = null;

		try {
			MetaInformacao metaInfo = completeMetaInformacao();

			URL url = CriarSessaoAutorizacaoParticipantev1.class
					.getResource(PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_WSDL));

			CriarSessaoAutorizacaoParticipantev1PortType port = getService(url)
					.getCriarSessaoAutorizacaoParticipantev1();

			((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_ENDPOINT));

			CriarSessaoAutorizacaoParticipanteInput input = setInput(request);

			response = port.criarSessaoAutorizacaoParticipante(input, metaInfo);

		} catch (ErroTecnicoFaultMsg | CriarSessaoAutorizacaoParticipanteFaultMsg e) {
			logger.error(e.getMessage(), e);
			LatamFidelidadeErrorHandler errorHandler = new LatamFidelidadeErrorHandler(e);
			throw new ChangeLoyaltyPremiumTicketsException(errorHandler.getCode(), errorHandler.getMessage(),
					errorHandler.getNativeMessage());

		} catch (WebServiceException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_MULTIPLUS_SERVICE_ID,
					PROPERTIES.getProperty(Constants.ERROR_MULTIPLUS_SERVICE), e.getMessage());

		} catch (ChangeLoyaltyPremiumTicketsException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(Constants.ERROR_NOT_CATALOGED_ID,
					PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED), e.getMessage());

		}

		return response;
	}

	public CriarSessaoAutorizacaoParticipanteInput setInput(ChangeLoyaltyPremiumTicketsRQDTO request) {

		SessaoAutorizacao sessaoAutorizacao = new SessaoAutorizacao();

		LoyaltyMemberDTO loyaltyMemberDTO = request.getLoyaltyMemberDTO();
		LoyaltyPremiumTicketsInfoDTO loyaltyPremiumTicketsInfoDTO = request.getLoyaltyPremiumTicketsInfoDTO();
		
		SessionContextDTO sessionContextDTO = loyaltyPremiumTicketsInfoDTO.getSessionContextDTO();
		SystemDTO systemDTO = sessionContextDTO.getSystemDTO();

		String ffNumber = loyaltyMemberDTO.getFfNumber();
		Participante participante = new Participante();
		participante.setNumeroMultiplus(ffNumber);
		sessaoAutorizacao.setParticipante(participante);

		String authorizationType = sessionContextDTO.getAuthorizationType();
		TipoReferencia referenciaAuthorization = new TipoReferencia();
		referenciaAuthorization.setValorReferencia(authorizationType);
		sessaoAutorizacao.setContextoAutorizacao(referenciaAuthorization);

		String originTransactionId = sessionContextDTO.getOriginTransactionId();
		Transacao transacao = new Transacao();
		transacao.setIdTransacao(originTransactionId);
		sessaoAutorizacao.setTransacaoOrigem(transacao);

		String idSistema = systemDTO.getSystemId();
		Sistema sistema = new Sistema();
		sistema.setIdSistema(idSistema);
		sessaoAutorizacao.setSistemaOrigem(sistema);

		String deviceId = systemDTO.getDeviceId();
		sessaoAutorizacao.setDeviceFingerprint(deviceId);

		List<ContextDTO> contexts = sessionContextDTO.getContexts();
		AtributoList atributoList = new AtributoList();

		for (ContextDTO context : contexts) {
			Atributo atributo = new Atributo();
			atributo.setChave(context.getKey());
			atributo.setValor(context.getValue());
			atributoList.getAtributo().add(atributo);
		}

		sessaoAutorizacao.setDadosAdicionais(atributoList);

		CriarSessaoAutorizacaoParticipanteInput input = new CriarSessaoAutorizacaoParticipanteInput();

		input.setSessaoAutorizacao(sessaoAutorizacao);

		return input;
	}

	public CriarSessaoAutorizacaoParticipantev1 getService(URL url) {
		synchronized (WSCreateSessionClient.class) {
			if (service == null) {
				service = new CriarSessaoAutorizacaoParticipantev1(url);
				service.setHandlerResolver(new HandlerResolver() {
					@SuppressWarnings("rawtypes")
					@Override
					public List<Handler> getHandlerChain(PortInfo portInfo) {
						List<Handler> handlerList = new ArrayList<>();
						handlerList.add(new WSSUsernameTokenSecurityHandler(
								PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_USER),
								PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_PASS)));
						return handlerList;
					}
				});
			}
			return service;
		}
	}

	/**
	 * Complete MetaInformacao from service properties
	 * 
	 * @return MetaInformacao mapped
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	private MetaInformacao completeMetaInformacao() throws ChangeLoyaltyPremiumTicketsException {

		MetaInformacao metaInformacao = new MetaInformacao();
		metaInformacao.setIdRequisicao(PROPERTIES.getProperty(Constants.WS_META_INFO_ID_REQUIREMENT));
		metaInformacao.setDataHoraRequisicao(DateUtil.obtainXMLGregorianCalendarFromSysdate());
		metaInformacao.setSistemaOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_SYSTEM));
		metaInformacao.setOperacaoOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_OPERATION));
		metaInformacao.setEnderecoOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_ORIGIN_ADDRESS));
		metaInformacao.setUsuarioOrigem(PROPERTIES.getProperty(Constants.WS_CREATE_SESSION_USER));
		metaInformacao.setCanalOrigem(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_VERSION));
		metaInformacao.setVersaoConsumidor(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_TYPE));
		metaInformacao
				.setVersaoServicoConsumido(PROPERTIES.getProperty(Constants.WS_META_INFO_SERVICE_CONSUMED_VERSION));
		metaInformacao.setTipoConsumidor(PROPERTIES.getProperty(Constants.WS_META_INFO_CONSUMER_TYPE));
		metaInformacao.setEnderecoServicoConsumido(
				PROPERTIES.getProperty(Constants.WS_META_INFO_SERVICE_CONSUMED_ADDRESS_CRIAR));

		return metaInformacao;
	}

}
