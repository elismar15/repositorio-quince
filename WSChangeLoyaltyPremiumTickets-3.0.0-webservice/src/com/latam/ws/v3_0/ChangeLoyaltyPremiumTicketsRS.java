
package com.latam.ws.v3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.latam.entities.v3_0.ServiceStatusType;
import com.latam.entities.v3_1.LoyaltyMemberType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceStatus" type="{http://entities.latam.com/v3_0}ServiceStatusType"/&gt;
 *         &lt;element name="LoyaltyPremiumTicket" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LoyaltyMember" type="{http://entities.latam.com/v3_1}LoyaltyMemberType"/&gt;
 *                   &lt;element name="Tickets"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Ticket" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Original"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Points"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="Exchanged"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                     &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                           &lt;element name="ReAccredited" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                     &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="Transaction"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="cancelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="usedId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="New"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="paxNameNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="receptId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "serviceStatus",
    "loyaltyPremiumTicket"
})
@XmlRootElement(name = "ChangeLoyaltyPremiumTicketsRS")
public class ChangeLoyaltyPremiumTicketsRS {

    @XmlElement(name = "ServiceStatus", required = true)
    protected ServiceStatusType serviceStatus;
    @XmlElement(name = "LoyaltyPremiumTicket")
    protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket loyaltyPremiumTicket;

    /**
     * Obtiene el valor de la propiedad serviceStatus.
     * 
     * @return
     *     possible object is
     *     {@link ServiceStatusType }
     *     
     */
    public ServiceStatusType getServiceStatus() {
        return serviceStatus;
    }

    /**
     * Define el valor de la propiedad serviceStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceStatusType }
     *     
     */
    public void setServiceStatus(ServiceStatusType value) {
        this.serviceStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad loyaltyPremiumTicket.
     * 
     * @return
     *     possible object is
     *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket }
     *     
     */
    public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket getLoyaltyPremiumTicket() {
        return loyaltyPremiumTicket;
    }

    /**
     * Define el valor de la propiedad loyaltyPremiumTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket }
     *     
     */
    public void setLoyaltyPremiumTicket(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket value) {
        this.loyaltyPremiumTicket = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LoyaltyMember" type="{http://entities.latam.com/v3_1}LoyaltyMemberType"/&gt;
     *         &lt;element name="Tickets"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Ticket" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Original"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Points"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Exchanged"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                           &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                                 &lt;element name="ReAccredited" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                           &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="Transaction"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="cancelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="usedId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="New"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="paxNameNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="receptId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "loyaltyMember",
        "tickets",
        "receptId"
    })
    public static class LoyaltyPremiumTicket {

        @XmlElement(name = "LoyaltyMember", required = true)
        protected LoyaltyMemberType loyaltyMember;
        @XmlElement(name = "Tickets", required = true)
        protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets tickets;
        protected String receptId;

        /**
         * Obtiene el valor de la propiedad loyaltyMember.
         * 
         * @return
         *     possible object is
         *     {@link LoyaltyMemberType }
         *     
         */
        public LoyaltyMemberType getLoyaltyMember() {
            return loyaltyMember;
        }

        /**
         * Define el valor de la propiedad loyaltyMember.
         * 
         * @param value
         *     allowed object is
         *     {@link LoyaltyMemberType }
         *     
         */
        public void setLoyaltyMember(LoyaltyMemberType value) {
            this.loyaltyMember = value;
        }

        /**
         * Obtiene el valor de la propiedad tickets.
         * 
         * @return
         *     possible object is
         *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets }
         *     
         */
        public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets getTickets() {
            return tickets;
        }

        /**
         * Define el valor de la propiedad tickets.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets }
         *     
         */
        public void setTickets(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets value) {
            this.tickets = value;
        }

        /**
         * Obtiene el valor de la propiedad receptId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReceptId() {
            return receptId;
        }

        /**
         * Define el valor de la propiedad receptId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReceptId(String value) {
            this.receptId = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Ticket" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Original"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Points"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Exchanged"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                                 &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                       &lt;element name="ReAccredited" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                                 &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="Transaction"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="cancelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="usedId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="New"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="paxNameNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ticket"
        })
        public static class Tickets {

            @XmlElement(name = "Ticket", required = true)
            protected List<ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket> ticket;

            /**
             * Gets the value of the ticket property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ticket property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTicket().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket }
             * 
             * 
             */
            public List<ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket> getTicket() {
                if (ticket == null) {
                    ticket = new ArrayList<ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket>();
                }
                return this.ticket;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Original"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Points"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Exchanged"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                       &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                             &lt;element name="ReAccredited" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                       &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="Transaction"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="cancelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="usedId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="New"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="paxNameNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "ticketNumber",
                "original",
                "_new"
            })
            public static class Ticket {

                @XmlElement(required = true)
                protected String ticketNumber;
                @XmlElement(name = "Original", required = true)
                protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original original;
                @XmlElement(name = "New", required = true)
                protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New _new;

                /**
                 * Obtiene el valor de la propiedad ticketNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTicketNumber() {
                    return ticketNumber;
                }

                /**
                 * Define el valor de la propiedad ticketNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTicketNumber(String value) {
                    this.ticketNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad original.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original }
                 *     
                 */
                public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original getOriginal() {
                    return original;
                }

                /**
                 * Define el valor de la propiedad original.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original }
                 *     
                 */
                public void setOriginal(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original value) {
                    this.original = value;
                }

                /**
                 * Obtiene el valor de la propiedad new.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New }
                 *     
                 */
                public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New getNew() {
                    return _new;
                }

                /**
                 * Define el valor de la propiedad new.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New }
                 *     
                 */
                public void setNew(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.New value) {
                    this._new = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="loyaltyCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="paxNameNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "loyaltyCertificateNumber",
                    "paxNameNumber"
                })
                public static class New {

                    @XmlElement(required = true)
                    protected String loyaltyCertificateNumber;
                    protected String paxNameNumber;

                    /**
                     * Obtiene el valor de la propiedad loyaltyCertificateNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLoyaltyCertificateNumber() {
                        return loyaltyCertificateNumber;
                    }

                    /**
                     * Define el valor de la propiedad loyaltyCertificateNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLoyaltyCertificateNumber(String value) {
                        this.loyaltyCertificateNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad paxNameNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPaxNameNumber() {
                        return paxNameNumber;
                    }

                    /**
                     * Define el valor de la propiedad paxNameNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPaxNameNumber(String value) {
                        this.paxNameNumber = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Points"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Exchanged"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                             &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                   &lt;element name="ReAccredited" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                             &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="Transaction"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="cancelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="usedId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "points",
                    "transaction"
                })
                public static class Original {

                    @XmlElement(name = "Points", required = true)
                    protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points points;
                    @XmlElement(name = "Transaction", required = true)
                    protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Transaction transaction;

                    /**
                     * Obtiene el valor de la propiedad points.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points }
                     *     
                     */
                    public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points getPoints() {
                        return points;
                    }

                    /**
                     * Define el valor de la propiedad points.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points }
                     *     
                     */
                    public void setPoints(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points value) {
                        this.points = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad transaction.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Transaction }
                     *     
                     */
                    public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Transaction getTransaction() {
                        return transaction;
                    }

                    /**
                     * Define el valor de la propiedad transaction.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Transaction }
                     *     
                     */
                    public void setTransaction(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Transaction value) {
                        this.transaction = value;
                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Exchanged"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                   &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *         &lt;element name="ReAccredited" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                   &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "exchanged",
                        "reAccredited"
                    })
                    public static class Points {

                        @XmlElement(name = "Exchanged", required = true)
                        protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.Exchanged exchanged;
                        @XmlElement(name = "ReAccredited")
                        protected ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.ReAccredited reAccredited;

                        /**
                         * Obtiene el valor de la propiedad exchanged.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.Exchanged }
                         *     
                         */
                        public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.Exchanged getExchanged() {
                            return exchanged;
                        }

                        /**
                         * Define el valor de la propiedad exchanged.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.Exchanged }
                         *     
                         */
                        public void setExchanged(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.Exchanged value) {
                            this.exchanged = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad reAccredited.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.ReAccredited }
                         *     
                         */
                        public ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.ReAccredited getReAccredited() {
                            return reAccredited;
                        }

                        /**
                         * Define el valor de la propiedad reAccredited.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.ReAccredited }
                         *     
                         */
                        public void setReAccredited(ChangeLoyaltyPremiumTicketsRS.LoyaltyPremiumTicket.Tickets.Ticket.Original.Points.ReAccredited value) {
                            this.reAccredited = value;
                        }


                        /**
                         * <p>Clase Java para anonymous complex type.
                         * 
                         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "amount",
                            "transactionId"
                        })
                        public static class Exchanged {

                            protected String amount;
                            @XmlElement(required = true)
                            protected String transactionId;

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAmount(String value) {
                                this.amount = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad transactionId.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getTransactionId() {
                                return transactionId;
                            }

                            /**
                             * Define el valor de la propiedad transactionId.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setTransactionId(String value) {
                                this.transactionId = value;
                            }

                        }


                        /**
                         * <p>Clase Java para anonymous complex type.
                         * 
                         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "amount",
                            "transactionId"
                        })
                        public static class ReAccredited {

                            protected String amount;
                            protected String transactionId;

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAmount(String value) {
                                this.amount = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad transactionId.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getTransactionId() {
                                return transactionId;
                            }

                            /**
                             * Define el valor de la propiedad transactionId.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setTransactionId(String value) {
                                this.transactionId = value;
                            }

                        }

                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="cancelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="usedId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "cancelId",
                        "usedId"
                    })
                    public static class Transaction {

                        @XmlElement(required = true)
                        protected String cancelId;
                        protected String usedId;

                        /**
                         * Obtiene el valor de la propiedad cancelId.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCancelId() {
                            return cancelId;
                        }

                        /**
                         * Define el valor de la propiedad cancelId.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCancelId(String value) {
                            this.cancelId = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad usedId.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getUsedId() {
                            return usedId;
                        }

                        /**
                         * Define el valor de la propiedad usedId.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setUsedId(String value) {
                            this.usedId = value;
                        }

                    }

                }

            }

        }

    }

}
