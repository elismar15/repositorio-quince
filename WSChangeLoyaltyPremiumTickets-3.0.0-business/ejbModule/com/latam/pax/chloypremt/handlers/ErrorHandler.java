/**
 * 
 */
package com.latam.pax.chloypremt.handlers;

import java.util.HashMap;
import java.util.Properties;

import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.chloypremt.utils.Constants;

import lombok.Getter;
import lombok.Setter;

/**
 * Error handler class
 * 
 * @author Alberto Tejos S.
 *
 */
public abstract class ErrorHandler {

	protected static final Properties PROPERTIES = AppConfigUtil.getApplicationProperties();
	protected static final String BLANK = " ";

	@Getter
	@Setter
	protected int code;

	@Getter
	@Setter
	protected String message;

	@Getter
	@Setter
	protected String nativeMessage;

	@Getter
	@Setter
	protected HashMap<Integer, ServiceError> serviceErrorMap;

	protected Exception exception;

	/**
	 * Constructor
	 * 
	 * @param e
	 *            Exception
	 */
	protected ErrorHandler(Exception e) {
		this.exception = e;
	}

	/**
	 * Fill the map with the errors corresponding to the customer to consume
	 */
	protected abstract void fillServiceErrorMap();

	/**
	 * Gets the error messages from the client exception and returns it in the
	 * correct format
	 */
	protected abstract String appendNativeMessage();

	/**
	 * Obtains the service error object from the error map
	 * 
	 * @param serviceErrorCode
	 * @return
	 */
	protected ServiceError getServiceError(int serviceErrorCode) {
		if (serviceErrorMap.containsKey(serviceErrorCode)) {
			return serviceErrorMap.get(serviceErrorCode);
		} else {
			return new ServiceError();
		}
	}

	/**
	 * Print native error message
	 * 
	 * @param error
	 * @return
	 */
	protected String printError(String error) {
		String outputError = BLANK;
		if (error != null && !error.isEmpty()) {
			outputError = error;
		}
		return outputError;
	}

	/**
	 * Represents the error codes and error messages of the parameters in the
	 * service specification document
	 * 
	 * @author Alberto Tejos S.
	 *
	 */
	class ServiceError {

		@Getter
		@Setter
		private int errorCode;

		@Getter
		@Setter
		private String errorMessage;

		public ServiceError() {
			this.errorCode = Constants.ERROR_NOT_CATALOGED_ID;
			this.errorMessage = PROPERTIES.getProperty(Constants.ERROR_NOT_CATALOGED);
		}

		public ServiceError(int errorCode, String errorMessage) {
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
		}
	}
}
