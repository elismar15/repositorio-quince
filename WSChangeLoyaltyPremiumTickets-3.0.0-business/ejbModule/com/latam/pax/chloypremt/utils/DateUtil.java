package com.latam.pax.chloypremt.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;
import lombok.extern.slf4j.Slf4j;

/**
 * Date utility class
 * 
 * @author Alberto Tejos S.
 */
@Slf4j
public final class DateUtil {

	private static final String CORRECT_FORMAT = ", the correct date format is: ";
	private static final String DATE_TIME_SLASHED_FORMAT = "dd/MM/yyyy hh:mm:ss";

	private DateUtil() {

	}

	/**
	 * Method that delivers formatted date system.
	 *
	 * @return the XML gregorian calendar
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static XMLGregorianCalendar obtainXMLGregorianCalendarFromSysdate()
			throws ChangeLoyaltyPremiumTicketsException {
		GregorianCalendar gcal = new GregorianCalendar();
		XMLGregorianCalendar xgcal = null;
		try {
			xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
		} catch (DatatypeConfigurationException e) {
			logger.error(" DatatypeConfigurationException ", e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getMessage());
		}

		return xgcal;
	}

	/**
	 * Transform String date to XMLGregorianCalendar
	 * 
	 * @param date
	 * @param dateFormat
	 * @return XMLGregorianCalendar
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static XMLGregorianCalendar stringDateToXMLGrogorianCalendar(String date, String dateFormat)
			throws ChangeLoyaltyPremiumTicketsException {
		XMLGregorianCalendar calendar = null;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

		try {
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(simpleDateFormat.parse(date));
			calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			calendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
			calendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		} catch (ParseException | DatatypeConfigurationException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getMessage() + CORRECT_FORMAT + DATE_TIME_SLASHED_FORMAT);
		}
		return calendar;
	}

}
