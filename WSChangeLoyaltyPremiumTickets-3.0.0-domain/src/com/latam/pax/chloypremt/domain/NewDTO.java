package com.latam.pax.chloypremt.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class NewDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Getter @Setter
	@NotNull(message="{}")
	private PassengerBookingTypeDTO passenger;
	
	@Getter @Setter
	private String pricePoints;
	
	@Getter @Setter
	private String loyaltyCertificateNumber;
	
	@Getter @Setter
	private String loyaltyCertificateCode;
	
	@Getter @Setter
	private String paxNameNumber;
}
