/**
 * 
 */
package com.latam.pax.chloypremt.utils;

import java.util.regex.PatternSyntaxException;

import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;

import lombok.extern.slf4j.Slf4j;

/**
 * String utility class
 * @author Alberto Tejos S.
 *
 */
@Slf4j
public final class StringUtil {

	private static final String HYPHEN = "-";

	private StringUtil() {
	}

	/**
	 * Verify if string is null or empty
	 * 
	 * @param string
	 * @return boolean
	 */
	public static boolean isNullOrEmpty(String string) {
		if (null != string && !string.isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Separates a string containing a hyphen. Example: 1-23233 with param part
	 * 2, return 23233
	 * 
	 * @param string
	 * @param part
	 * @return string part
	 * @throws ChangeLoyaltyPremiumTicketsException
	 */
	public static String hyphenSplit(String string, int part) throws ChangeLoyaltyPremiumTicketsException {
		try {
			String[] parts = string.split(HYPHEN);
			return parts[part];
		} catch (PatternSyntaxException e) {
			logger.error(e.getMessage(), e);
			throw new ChangeLoyaltyPremiumTicketsException(e.getMessage());
		}
	}
}
