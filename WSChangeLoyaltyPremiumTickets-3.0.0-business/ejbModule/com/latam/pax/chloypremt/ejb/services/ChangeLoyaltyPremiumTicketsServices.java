package com.latam.pax.chloypremt.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRQDTO;
import com.latam.pax.chloypremt.domain.ChangeLoyaltyPremiumTicketsRSDTO;
import com.latam.pax.chloypremt.exceptions.ChangeLoyaltyPremiumTicketsException;

/**
 * Service interface of ChangeLoyaltyPremiumTickets-3.0.0
 * 
 * @author ANT using import-ws target
 * @author LATAM Enterprise Architect
 * @author Alberto Tejos S.
 */
@Local
public interface ChangeLoyaltyPremiumTicketsServices {

	/**
	 * Method that must have all business logic. All objects between Webservice
	 * layer and Business Layer must be declared as business domain (-domain
	 * project) (In some specific cases the business domain can be equal to the
	 * webservice domain)
	 * 
	 * @param businessData
	 * @return
	 * @throws LATAMException
	 */
	public ChangeLoyaltyPremiumTicketsRSDTO execute(ChangeLoyaltyPremiumTicketsRQDTO businessData)
			throws ChangeLoyaltyPremiumTicketsException;
}
