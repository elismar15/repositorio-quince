package com.latam.pax.chloypremt.exceptions;

/**
 * Service exception class
 * @author Alberto Tejos S.
 *
 */
public class ChangeLoyaltyPremiumTicketsException extends Exception {

	private static final long serialVersionUID = 1L;

	private int code;
	private String nativeMessage;
	
	public ChangeLoyaltyPremiumTicketsException() {
		super();
	}
	
	public ChangeLoyaltyPremiumTicketsException(String message) {
		super(message);
	}
	
	public ChangeLoyaltyPremiumTicketsException(int code, String message) {
		super(message);
		this.code = code;
	}
	
	public ChangeLoyaltyPremiumTicketsException(int code, String message, String nativeMessage) {
		super(message);
		this.code = code;
		this.nativeMessage = nativeMessage;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getNativeMessage() {
		return nativeMessage;
	}

	public void setNativeMessage(String nativeMessage) {
		this.nativeMessage = nativeMessage;
	}

}
