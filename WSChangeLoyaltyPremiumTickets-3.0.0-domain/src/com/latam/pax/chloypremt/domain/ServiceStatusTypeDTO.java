/**
 * 
 */
package com.latam.pax.chloypremt.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class ServiceStatusTypeDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Getter @Setter
	@NotNull(message="{}")
	private int code;
	
	@Getter @Setter
	private String message;
	
	@Getter @Setter
	private String nativeMessage;

}
