package com.latam.pax.chloypremt.handlers;

import java.util.Set;
import java.util.TreeSet;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * This class allows you to dynamically security service invokes.
 *
 * @author Alberto Tejos S.
 */
@Slf4j
public class WSSUsernameTokenSecurityHandler implements SOAPHandler<SOAPMessageContext> {

	@Getter
	@Setter
	private String user;

	@Getter
	@Setter
	private String pass;

	/**
	 * Instantiates a new WSS username token security handler.
	 *
	 * @param user
	 *            the user
	 * @param pass
	 *            the pass
	 */
	public WSSUsernameTokenSecurityHandler(String user, String pass) {
		setUser(user);
		setPass(pass);
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) throws WebServiceException {
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty.booleanValue()) {
			try {
				SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
				SOAPFactory factory = SOAPFactory.newInstance();
				String prefix = "wsse";
				String uri = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
				SOAPElement securityElem = factory.createElement("Security", prefix, uri);
				SOAPElement tokenElem = factory.createElement("UsernameToken", prefix, uri);
				tokenElem.addAttribute(QName.valueOf("wsu:Id"), "UsernameToken-2");
				tokenElem.addAttribute(QName.valueOf("xmlns:wsu"),
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
				SOAPElement userElem = factory.createElement("Username", prefix, uri);
				userElem.addTextNode(getUser());
				SOAPElement pwdElem = factory.createElement("Password", prefix, uri);
				pwdElem.addTextNode(getPass());
				pwdElem.addAttribute(QName.valueOf("Type"),
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
				tokenElem.addChildElement(userElem);
				tokenElem.addChildElement(pwdElem);
				securityElem.addChildElement(tokenElem);
				if (null != envelope.getHeader()) {
					envelope.getHeader().detachNode();
				}

				SOAPHeader header = envelope.addHeader();
				header.addChildElement(securityElem);

			} catch (SOAPException e) {
				if (logger.isErrorEnabled()) {
					logger.error("Exception in SOAPException ", e);
				}
				throw new WebServiceException(e.getMessage());
			}
		}
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		return new TreeSet<>();
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return false;
	}

	@Override
	public void close(MessageContext context) {
		//
	}

}
