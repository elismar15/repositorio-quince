package com.latam.pax.chloypremt.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Alberto Tejos S.
 *
 */
@EqualsAndHashCode
@ToString
public class OriginalDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Getter @Setter
	@NotNull(message="{}")
	private PassengerBookingTypeDTO passengerBookingTypeDTO;
	
	@Getter @Setter
	private PointsDTO pointsDTO;
	
	@Getter @Setter
	private String loyaltyCertificateNumber;
	
	@Getter @Setter
	private TransactionDTO transactionDTO;
}
